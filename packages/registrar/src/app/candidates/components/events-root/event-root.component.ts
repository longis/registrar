import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-enrollment-event-root',
  templateUrl: './event-root.component.html'
})
export class EnrollmentEventRootComponent implements OnInit {

  public model: any;
  public tabs: any[];
  public enrollmentEventId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.enrollmentEventId = this._activatedRoute.snapshot.params.id;

    this.model = await this._context.model('StudyProgramEnrollmentEvents')
      .where('id').equal(this.enrollmentEventId)
      .expand('inscriptionPeriod($expand=locale)')
      .getItem();

    this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');

    // save user activity for events
    return this._userActivityService.setItem({
      category: this._translateService.instant('Candidates.TitleSingular'),
      description: this._translateService.instant(this.model.name),
      url: window.location.hash.substring(1),
      dateCreated: new Date()
    });
  }

}
