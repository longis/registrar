import {
  Component,
  OnInit,
  OnDestroy,
} from '@angular/core';

@Component({
  selector: 'app-candidate-upload-actions-home',
  templateUrl: './candidate-upload-actions-home.component.html',
})
export class CandidateUploadActionsHomeComponent implements OnInit, OnDestroy {
  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {}
}

