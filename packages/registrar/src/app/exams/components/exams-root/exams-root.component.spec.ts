import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsRootComponent } from './exams-root.component';

describe('ExamsRootComponent', () => {
  let component: ExamsRootComponent;
  let fixture: ComponentFixture<ExamsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
