import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { Subscription, combineLatest } from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
import { LoadingService } from '@universis/common';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-exams-preview-students',
  templateUrl: './exams-preview-students.component.html',
  styleUrls: []
})
export class ExamsPreviewStudentsComponent implements OnInit, OnDestroy {

  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public examId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  // @Input() tableConfiguration: any;
  // @Input() searchConfiguration: any;
  private subscription: Subscription;
  public courseExam: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _templateManageService: TemplateManageService
  ) { }

  async ngOnInit() {
    this._templateManageService.updateShowStatus(false);
    this.subscription = combineLatest(
      this._activatedRoute.params,
      this._activatedRoute.data
    ).subscribe(async (results) => {
      const params = results[0];
      const data: any = results[1];
      this.examId = params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('CourseExams/' + this.examId + '/students')
        .asQueryable()
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
      this.students.fetch();
      // declare model for advance search filter criteria
      this.students.config.model = 'CourseExams/' + params.id + '/students';

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // get course exam
      this.courseExam = await this._context.model('CourseExams')
        .where('id').equal(this.examId)
        .select('id', 'name', 'year', 'course', 'examPeriod')
        .expand('course', 'year', 'examPeriod')
        .getItem();
    });
  }

  exportGradeTable(): void {
    this._loadingService.showLoading();
    const headers = new Headers();
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // set accept header.
    headers.set('Accept', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    const fileURL = this._context.getService().resolve(`/CourseExams/${this.examId}/exportStudents`);
    // fetch exam grading sheet.
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      return response.blob();
    }).then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        const name = this.courseExam.name.replace(/[/\\?%*:|"<>]/g, '-');
        const extension = 'xlsx';
        const downloadName = `${this.courseExam.year.name}_${this.courseExam.examPeriod.name}_${this.courseExam.course.displayCode}_${name}.${extension}`;
        a.download = downloadName;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loadingService.hideLoading();
      });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    this._templateManageService.updateShowStatus(true);


  }

}
