import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { Subscription, combineLatest } from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-exams-preview-participations',
  templateUrl: './exams-preview-participations.component.html'
})
export class ExamsPreviewParticipationsComponent implements OnInit, OnDestroy {

  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public examId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  // @Input() tableConfiguration: any;
  // @Input() searchConfiguration: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _context: AngularDataContext,
    private _activeDepartmentService: ActiveDepartmentService,
    private _templateManageService: TemplateManageService
  ) { }

  async ngOnInit() {
    this._templateManageService.updateShowStatus(false);
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    this.subscription = combineLatest(this._activatedRoute.params, this._activatedRoute.data).subscribe(async (results) => {
      const params = results[0];
      const data = results[1];
      this.examId = params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('CourseExamParticipateActions')
        .where('courseExam')
        .equal(this.examId)
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
      this.students.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.formComponent.formLoad.subscribe((res: any) => {
          Object.assign(res, { department: activeDepartment });
        });
        this.search.ngOnInit();
      }
      
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    this._templateManageService.updateShowStatus(true);
  }

}
