import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TemplateManageService } from '@longis/ngx-longis/registrar';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-course-exam-upload-actions',
  templateUrl: './course-exam-upload-actions.component.html',
  styleUrls: ['./course-exam-upload-actions.component.scss']
})
export class CourseExamUploadActionsComponent implements OnInit, OnDestroy  {

  public uploadGrades: any;
  public active: number;
  public examId: any;
  private subscription: Subscription;
  private currentStudyProgramSubscription: Subscription;
  private paramSubscription: Subscription;
  public studyProgram: number;
  public specialization: number;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext, 
    private _templateManageService: TemplateManageService
  ) { }

  async ngOnInit() {
    this.currentStudyProgramSubscription = this._templateManageService.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram);
    this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
      if (params && params.specialization) {
        this.specialization = params.specialization;
      }
    });
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.examId = params.id;
      this.uploadGrades = await this._context.model('CourseExams/' + params.id + '/actions')
        .asQueryable()
        .where('actionStatus/alternateName').equal('CompletedActionStatus')
        .or('actionStatus/alternateName').equal('FailedActionStatus')
        .or('actionStatus/alternateName').equal('ActiveActionStatus')
        .prepare().and('additionalResult').notEqual(null)
        .expand('owner,additionalResult,grades($select=action,count(id) as totalCount;$groupby=action)')
        .orderByDescending('dateCreated')
        .take(-1)
        .getItems();

      this.active = (this.uploadGrades).filter(x => {
        return x.actionStatus && x.actionStatus.alternateName === 'ActiveActionStatus';
      }).length;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.currentStudyProgramSubscription) {
      this.currentStudyProgramSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
