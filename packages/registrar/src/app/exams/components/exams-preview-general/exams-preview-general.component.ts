import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TemplateManageService } from '@longis/ngx-longis/registrar';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-exams-preview-general',
  templateUrl: './exams-preview-general.component.html',
  styleUrls: ['./exams-preview-general.component.scss']
})
export class ExamsPreviewGeneralComponent implements OnInit, OnDestroy {

  public model: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _templateManageService: TemplateManageService) { }

  async ngOnInit() {
    this._templateManageService.updateShowStatus(false);
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('CourseExams')
        .where('id').equal(params.id)
        .expand('course($expand=courseStructureType, instructor, department, gradeScale)')
        .getItem();
    });
    }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this._templateManageService.updateShowStatus(true);
  }
}
