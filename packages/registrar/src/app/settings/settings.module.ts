import { NgModule, OnInit, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings.routing';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import {ActivatedUser, ConfigurationService, SharedModule} from '@universis/common';
import { TablesModule } from '@universis/ngx-tables';
import { MostModule, AngularDataContext } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ListComponent } from './components/list/list.component';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';
import { SectionsComponent } from './components/sections/sections.component';
import { SettingsService, SettingsSection } from '../settings-shared/services/settings.service';
import sourceAt = require('lodash/get');
import { Subscription } from 'rxjs';
import { RouterModalModule } from '@universis/common/routing';
import { NgArrayPipesModule } from 'ngx-pipes';
import {TemplatePipe} from '@universis/common';
import { CandidateSourcesAttachSchemaComponent } from './components/candidate-sources/candidate-sources-attach-schema/candidate-sources-attach-schema.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AdvancedListWrapperComponent } from './components/candidate-sources/advanced-list-wrapper/advanced-list-wrapper.component';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    TablesModule,
    MostModule,
    TranslateModule,
    SettingsRoutingModule,
    SettingsSharedModule,
    RouterModalModule,
    NgArrayPipesModule,
    NgxDropzoneModule,
    RegistrarSharedModule
  ],
  declarations: [
    HomeComponent,
    ListComponent,
    SectionsComponent,
    CandidateSourcesAttachSchemaComponent,
    AdvancedListWrapperComponent
  ],
  providers: [TemplatePipe]
})
export class SettingsModule implements OnDestroy {

  private subscription: Subscription;

  constructor(private _context: AngularDataContext,
    private _settings: SettingsService,
    private _translateService: TranslateService,
    private _activatedUser: ActivatedUser,
    private _template: TemplatePipe,
    private _configurationService: ConfigurationService) {
    this.subscription = this._activatedUser.user.subscribe( user => {
      if (user) {
        this.initialize();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  private async initialize() {
    const metadata = await this._context.getMetadata();
    const translations = this._translateService.instant('Settings.Lists');
    const items = metadata.EntityType.filter( x => {
      return x.ImplementsType === 'Enumeration';
    }).filter((x) => {
      const entitySet = metadata.EntityContainer.EntitySet.find( y => {
        return y.EntityType === x.Name;
      });
      return entitySet != null;
    }).map( x =>  {
      // get long description fallback
      // use it if translation is missing
      const description = sourceAt(translations, `${x.Name}.Description`) || x.Name;
      const longDescriptionFallback = this._template.transform(translations.LongDescriptionFallback,
        {Description: description},
        {interpolate:  /{{([\s\S]+?)}}/g } );
      const entitySet = metadata.EntityContainer.EntitySet.find( y => {
        return y.EntityType === x.Name;
      });
      // use lodash.get in order to allow translation fallback
      return <SettingsSection> {
        category: 'Lists',
        name: x.Name,
        description: description,
        longDescription: sourceAt(translations, `${x.Name}.LongDescription`) || longDescriptionFallback,
        url: `/settings/lists/${entitySet.Name}`
      };
    });
    this._settings.addSection.apply(this._settings, items);

    // Sections from configuration
    const configurationSettings = (this._configurationService.settings as any);
    if (configurationSettings.sections && Array.isArray(configurationSettings.sections)) {
      const configurationSections = configurationSettings.sections.map(item => ({
        ...item,
        name: this._translateService.instant(item.name),
        description: this._translateService.instant(item.description),
        longDescription: this._translateService.instant(item.longDescription)
      }));
      this._settings.addSection(...configurationSections);
    }

    // CourseTypes
    const longDescriptionFallbackForCourseTypes = this._template.transform(translations.LongDescriptionFallback,
      {Description: translations.CourseTypes.Description},
      {interpolate:  /{{([\s\S]+?)}}/g } );
    const types = [<SettingsSection> {
      category: 'Lists',
      name: translations.CourseTypes.Description,
      description: translations.CourseTypes.Description,
      longDescription: longDescriptionFallbackForCourseTypes,
      url: `/settings/lists/CourseTypes`
    }];
    this._settings.addSection.apply(this._settings, types);

    // CourseAreas
    const longDescriptionFallbackForCourseAreas = this._template.transform(translations.LongDescriptionFallback,
      {Description: translations.CourseAreas.Description},
      {interpolate:  /{{([\s\S]+?)}}/g } );
    const areas = [<SettingsSection> {
      category: 'Lists',
      name: 'CourseAreas',
      description: translations.CourseAreas.Description,
      longDescription: longDescriptionFallbackForCourseAreas,
      url: `/settings/lists/CourseAreas`
    }];
    this._settings.addSection.apply(this._settings, areas);

    // CourseSectors
    const longDescriptionFallbackForCourseSectors = this._template.transform(translations.LongDescriptionFallback,
      {Description: translations.CourseSectors.Description},
      {interpolate:  /{{([\s\S]+?)}}/g } );
    const sectors = [<SettingsSection> {
      category: 'Lists',
      name: 'CourseSectors',
      description: translations.CourseSectors.Description,
      longDescription: longDescriptionFallbackForCourseSectors,
      url: `/settings/lists/CourseSectors`
    }];
    this._settings.addSection.apply(this._settings, sectors);

    // ExamPeriods
    const longDescriptionFallbackForExamPeriods = this._template.transform(translations.LongDescriptionFallback,
      {Description: translations.ExamPeriods.Description},
      {interpolate:  /{{([\s\S]+?)}}/g } );
    const examPeriods = [<SettingsSection> {
      category: 'Lists',
      name: 'ExamPeriods',
      description: translations.ExamPeriods.Description,
      longDescription: longDescriptionFallbackForExamPeriods,
      url: `/settings/lists/ExamPeriods`
    }];
    this._settings.addSection.apply(this._settings, examPeriods);

    // ThesisSubjects
    const longDescriptionFallbackForThesisSubjects = this._template.transform(translations.LongDescriptionFallback,
      {Description: translations.ThesisSubjects.Description},
      {interpolate:  /{{([\s\S]+?)}}/g } );
    const thesisSubjets = [<SettingsSection> {
      category: 'Lists',
      name: 'ThesisSubjects',
      description: translations.ThesisSubjects.Description,
      longDescription: longDescriptionFallbackForThesisSubjects,
      url: `/settings/lists/ThesisSubjects`
    }];
    this._settings.addSection.apply(this._settings, thesisSubjets);


    // ActionStatusArticles
    const longDescriptionFallbackForActionStatusArticles = this._template.transform(translations.LongDescriptionFallback,
      {Description: translations.ActionStatusArticles.Description},
      {interpolate:  /{{([\s\S]+?)}}/g } );
    const ActionStatusArticles = [<SettingsSection> {
      category: 'Lists',
      name: 'ActionStatusArticles',
      description: translations.ActionStatusArticles.Description,
      longDescription: longDescriptionFallbackForActionStatusArticles,
      url: `/settings/lists/ActionStatusArticles`
    }];
    this._settings.addSection.apply(this._settings, ActionStatusArticles);


    // ThesisSubjects
    const longDescriptionFallbackForNationalities  = this._template.transform(translations.LongDescriptionFallback,
      {Description: translations.Nationalities.Description},
      {interpolate:  /{{([\s\S]+?)}}/g } );
    const nationalities = [<SettingsSection> {
      category: 'Lists',
      name: 'Nationalities',
      description: translations.Nationalities.Description,
      longDescription: longDescriptionFallbackForNationalities,
      url: `/settings/lists/Nationalities`
    }];
    this._settings.addSection.apply(this._settings, nationalities);

    // Places
    const longDescriptionFallbackForPlaces = this._template.transform(translations.LongDescriptionFallback,
      { Description: translations.Places.Description },
      { interpolate: /{{([\s\S]+?)}}/g });
    const Places = [<SettingsSection>{
      category: 'Lists',
      name: 'Places',
      description: translations.Places.Description,
      longDescription: longDescriptionFallbackForPlaces,
      url: `/settings/lists/Places`
    }];
    this._settings.addSection.apply(this._settings, Places);

  }
}
