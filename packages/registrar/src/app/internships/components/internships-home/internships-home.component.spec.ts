import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternshipsHomeComponent } from './internships-home.component';

describe('InternshipsHomeComponent', () => {
  let component: InternshipsHomeComponent;
  let fixture: ComponentFixture<InternshipsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternshipsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternshipsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
