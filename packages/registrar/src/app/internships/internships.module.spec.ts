import { InternshipsModule } from './internships.module';
import {inject} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';

describe('InternshipsModule', () => {

  beforeEach(() => {
    //
  });

  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const internshipsModule = new InternshipsModule(translateService);
    expect(internshipsModule).toBeTruthy();
  }));
});
