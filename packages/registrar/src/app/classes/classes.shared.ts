import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ClassesFormComponent} from './components/classes-dashboard/classes-form/classes-form.component';
import {AppEventService, SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import { NewEventInstructorResolver, NewEventCourseClassResolver, CourseClassTimetableResolver, CurrentCourseClassResolver } from './classes.resolvers';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    ClassesFormComponent
  ],
  providers: [
    CourseClassTimetableResolver,
    CurrentCourseClassResolver,
    NewEventCourseClassResolver,
    NewEventInstructorResolver
  ],
  exports: [
    ClassesFormComponent
  ]
})
export class ClassesSharedModule {

  constructor(private _translateService: TranslateService, private appEvent: AppEventService) {
    const sources = environment.languages.map((language: string) => {
      return import(`./i18n/classes.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
        return Promise.resolve();
      })
    });
    Promise.all(sources).then(() => {
      // emit event
      this.appEvent.add.next({
        service: this._translateService,
        type: this._translateService.setTranslation
      });
    }).catch((err) => {
      console.error('An error occurred while loading shared module');
      console.error(err);
    });
  }

}
