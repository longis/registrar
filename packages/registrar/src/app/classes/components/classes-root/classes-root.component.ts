import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {cloneDeep} from 'lodash';
import {AppEventService, TemplatePipe} from '@universis/common';
import {Subscription} from 'rxjs';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';
import { AdvancedTableConfiguration } from '@universis/ngx-tables';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-classes-root',
  templateUrl: './classes-root.component.html',
})
export class ClassesRootComponent implements OnInit, OnDestroy  {
  public model: any;
  public classId: any;
  public config: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;
  private studyProgram: number;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe,
              private _appEvent: AppEventService,
              private _activeDepartmentService: ActiveDepartmentService,
              private resolver: TableConfigurationResolver, 
              private _templateManageService: TemplateManageService, 
              private _route: Router) { }

  ngOnInit() {
    this._templateManageService.updateShowStatus(false);
    this._activatedRoute.parent.params.subscribe(async(params) => {
      this.studyProgram = params.id;
    })
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.classId = params.id;

      this.model = await this._context.model('CourseClasses')
        .where('id').equal(params.id)
        .expand('course,status,period($expand=locale)')
        .getItem();

      this.resolver.get('CourseClasses').subscribe((config) => {
        // @ts-ignore
        this.config = AdvancedTableConfiguration.cast(config);
        if (this.config.columns && this.model) {
          // get actions from config file
          const actions = this.config.columns.filter(x => {
            return x.actions;
          })
            // map actions
            .map(x => x.actions)
            // get list items
            .reduce((a, b) => b, 0);
            actions.forEach(action => {
              if (action.href) {
                action.href = (this.cutUrl(this._route.url, action.href) === action.href ? action.href :  this.cutUrl(this._route.url, action.href) + (action.href[0] === '#' ? action.href.slice(2) : null))
              }
            })

          // filter actions with student permissions
          this._activeDepartmentService.getActiveDepartment().then((activeDepartment) => {
            // filter actions by current department
            this.allowedActions = actions.slice().filter(x => {
              if (x.role) {
                if (x.access && x.access.length > 0) {
                  let access = x.access;
                  access = access.filter(y => {
                    if (y.department && y.department === 'current') {
                      return this.model.course.department === activeDepartment.id;
                    } else {
                      return true;
                    }
                  });
                  if (access && access.length > 0) {
                    return x;
                  }
                } else {
                  return x;
                }
              }
            });
            this.edit = this.allowedActions.find(x => {
              if (x.role === 'edit') {
                x.href = this._template.transform(x.href, this.model);
                return x;
              }
            });
            this.actions = this.allowedActions;
            this.actions.forEach(action => {
              action.href = this._template.transform(action.href, this.model);
            });
          });
        }
      });
    });
  }

  cutUrl(str, href) {
    var matched = str.match(/([^/]*\/){6}/);
    return matched ? ('#' + matched[0]) : href;
}

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this._templateManageService.updateShowStatus(true);
  }
}
