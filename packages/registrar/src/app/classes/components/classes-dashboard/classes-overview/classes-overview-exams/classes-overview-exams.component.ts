import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TemplateManageService } from '@longis/ngx-longis/registrar';
import { AngularDataContext } from '@themost/angular';
import { ReferrerRouteParams, ReferrerRouteService } from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-overview-exams',
  templateUrl: './classes-overview-exams.component.html',
  styleUrls: ['./classes-overview-exams.component.scss']
})
export class ClassesOverviewExamsComponent implements OnInit, OnDestroy {

  public classExams: any;
  private subscription: Subscription;
  private currentStudyProgramSubscription: Subscription;
  private paramSubscription: Subscription;
  public studyProgram: number;
  public specialization: number;
  referrerSubscription: Subscription;
  referrerRoute: ReferrerRouteParams = {
    commands: ['']
  }

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext, 
              private _templateManageService: TemplateManageService,
              private _referrer: ReferrerRouteService) { }

  async ngOnInit() {
    this.currentStudyProgramSubscription = this._templateManageService.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram);
    const studyProgramPeriod = await this._context.model("StudyPrograms").where('id').equal(this.studyProgram)
    .select('currentPeriod').getItem();
    this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
      if (params && params.specialization) {
        this.specialization = params.specialization;
      }
    });
    if(studyProgramPeriod && studyProgramPeriod.currentPeriod && studyProgramPeriod.currentPeriod.id){
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.classExams = await this._context.model('CourseExamClasses')
        .where('courseClass').equal(params.id).and('courseClass/studyProgram/id').equal(this.studyProgram).and('courseExam/examPeriod').equal(studyProgramPeriod.currentPeriod.id)
        .expand('courseExam($expand=examPeriod,status,completedByUser,year,course($expand=department)),courseClass($expand=studyProgram)')
        .orderByDescending('courseExam/year')
        .thenByDescending('courseExam/examPeriod')
        .getItems();
    });
  }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.currentStudyProgramSubscription) {
      this.currentStudyProgramSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
