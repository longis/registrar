import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassesHomeComponent } from './components/classes-home/classes-home.component';
import {ClassesTableComponent } from './components/classes-table/classes-table.component';
import { ClassesRootComponent } from './components/classes-root/classes-root.component';
import {COLUMN_FORMATTERS, TablesModule} from '@universis/ngx-tables';
import {TranslateModule} from '@ngx-translate/core';
import {ClassesRoutingModule} from './classes.routing';
import {ClassesSharedModule} from './classes.shared';
import { ClassesStudentsComponent } from './components/classes-dashboard/classes-students/classes-students.component';
import {ConfigurationService, SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {StudentsSharedModule} from '../students/students.shared';
import {InstructorsSharedModule} from '../instructors/instructors.shared';
import { ClassesInstructorsComponent } from './components/classes-dashboard/classes-instructors/classes-instructors.component';
import {ElementsModule} from '../elements/elements.module';
// tslint:disable-next-line: max-line-length
import {ClassesStudentAdvancedTableSearchComponent} from './components/classes-dashboard/classes-students/classes-student-advanced-table-search.component';
import {MostModule} from '@themost/angular';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import {ClassesAddInstructorComponent} from './components/classes-dashboard/classes-instructors/classes-add-instructor.component';
import {ClassesAddStudentComponent} from './components/classes-dashboard/classes-students/classes-add-student.component';
import {RouterModalModule} from '@universis/common/routing';
import {TooltipModule} from 'ngx-bootstrap';
import { ClassesOverviewComponent } from './components/classes-dashboard/classes-overview/classes-overview.component';
import { ClassesDashboardComponent } from './components/classes-dashboard/classes-dashboard.component';
import { CoursesSharedModule } from '../courses/courses.shared';
import { ClassesCoursesComponent } from './components/classes-dashboard/classes-courses/classes-courses.component';
// tslint:disable-next-line:max-line-length
import { ClassesOverviewGeneralComponent } from './components/classes-dashboard/classes-overview/classes-overview-general/classes-overview-general.component';
// tslint:disable-next-line:max-line-length
import { ClassesOverviewSectionsComponent } from './components/classes-dashboard/classes-overview/classes-overview-sections/classes-overview-sections.component';
// tslint:disable-next-line:max-line-length
import { ClassesOverviewStatsOtherRegistrationsComponent } from './components/classes-dashboard/classes-overview/classes-overview-stats-other-registrations/classes-overview-stats-other-registrations.component';
import {ChartsModule} from 'ng2-charts';
// tslint:disable-next-line:max-line-length
import { ClassesOverviewExamsComponent } from './components/classes-dashboard/classes-overview/classes-overview-exams/classes-overview-exams.component';
import { ExamsSharedModule } from '../exams/exams.shared';
// tslint:disable-next-line:max-line-length
import { ClassesOverviewStatsAutoregisteredComponent } from './components/classes-dashboard/classes-overview/classes-overview-stats-autoregistered/classes-overview-stats-autoregistered.component';
import { ClassesExamsComponent } from './components/classes-dashboard/classes-exams/classes-exams.component';
// tslint:disable-next-line:max-line-length
import { ClassesInstructorAdvancedTableSearchComponent } from './components/classes-dashboard/classes-instructors/classes-instructor-advanced-table-search.component';
import {ClassesSectionsComponent} from './components/classes-dashboard/classes-sections/classes-sections.component';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';
import {RulesModule} from '../rules';
import { ClassesBooksComponent } from './components/classes-dashboard/classes-books/classes-books.component';
import { AdminSharedModule } from '@universis/ngx-admin';
import { EventsModule } from '@universis/ngx-events';
import { ExtendedColumnsFactory } from '@universis/ngx-events';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ClassesSharedModule,
    TablesModule,
    ClassesRoutingModule,
    EventsModule, // add events module after routing to override routing provided by ngx-events
    SharedModule,
    FormsModule,
    StudentsSharedModule,
    InstructorsSharedModule,
    ElementsModule,
    MostModule,
    RegistrarSharedModule,
    RouterModalModule,
    TooltipModule.forRoot(),
    CoursesSharedModule,
    ExamsSharedModule,
    ChartsModule,
    ReportsSharedModule,
    RulesModule,
    AdminSharedModule
  ],
  declarations: [
    ClassesHomeComponent,
    ClassesRootComponent,
    ClassesTableComponent,
    ClassesStudentsComponent,
    ClassesInstructorsComponent,
    ClassesStudentAdvancedTableSearchComponent,
    ClassesAddInstructorComponent,
    ClassesAddStudentComponent,
    ClassesOverviewComponent,
    ClassesDashboardComponent,
    ClassesCoursesComponent,
    ClassesOverviewGeneralComponent,
    ClassesOverviewSectionsComponent,
    ClassesOverviewStatsOtherRegistrationsComponent,
    ClassesOverviewExamsComponent,
    ClassesOverviewStatsAutoregisteredComponent,
    ClassesExamsComponent,
    ClassesInstructorAdvancedTableSearchComponent,
    ClassesSectionsComponent,
    ClassesBooksComponent
  ],
  providers: [
    {
      provide: COLUMN_FORMATTERS,
      useFactory: ExtendedColumnsFactory,
      deps: [ConfigurationService]
    }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ClassesModule { }
