import {Injectable} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class UserDepartmentsService {

  constructor(private _context: AngularDataContext) {
  }

  private getusersDepartments(): any {
   return this._context.model('Users/me/departments')
      .asQueryable()
      .expand("currentPeriod($expand=locale)")
      .take(-1)
      .getItems();
  }

  private getLocalDepartments(): any {
   return this._context.model('LocalDepartments')
      .asQueryable()
      .expand("currentPeriod($expand=locale)")
      .take(-1)
      .getItems();
  }

  async getDepartments() {
    const usersDepartments = await this.getusersDepartments();
    const localDepartments = await this.getLocalDepartments();
    return  localDepartments.filter( localdepartment => {
      return  usersDepartments.find( x => x.id ===  localdepartment.id);
    });
  }
}
