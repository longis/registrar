import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { AngularDataContext } from "@themost/angular";
import { ConfigurationService } from "@universis/common";
import { AdvancedFormItemResolver } from "@universis/forms";

@Injectable()
export class AdvancedFormStudentWithLocalesResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _itemResolver: AdvancedFormItemResolver,
              private _configurationService: ConfigurationService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    return this._itemResolver.resolve(route, state).then((item) => {
      if (item.person.locales) {
        // get index of object with inLanguage: defaultLocale
        const removeIndex = item.person.locales.findIndex(x => {
          return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
        });
        // remove object
        if (removeIndex >= 0) {
          item.person.locales.splice(removeIndex, 1);
        }

        // map application locales
        for (const locale of this._configurationService.settings.i18n.locales) {
          if (locale !== this._configurationService.settings.i18n.defaultLocale) {
            // try to find  if localization data exist
            const findLocale = item.person.locales.find((itemLocale) => {
              return itemLocale.inLanguage === locale;
            });
            if (findLocale == null) {
              // if item does not exist add it
              item.person.locales.push({
                inLanguage: locale
              });
            }
          }
        }
      }
      return Promise.resolve(item);
    });
  }
}

@Injectable()
export class AdvancedFormStudyProgramWithLocalesResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _itemResolver: AdvancedFormItemResolver,
              private _configurationService: ConfigurationService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    return this._itemResolver.resolve(route, state).then((item) => {
      if (item.info.locales) {
        // get index of object with inLanguage: defaultLocale
        const removeIndex = item.info.locales.findIndex(x => {
          return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
        });
        // remove object
        if (removeIndex >= 0) {
          item.info.locales.splice(removeIndex, 1);
        }

        // map application locales
        for (const locale of this._configurationService.settings.i18n.locales) {
          if (locale !== this._configurationService.settings.i18n.defaultLocale) {
            // try to find  if localization data exist
            const findLocale = item.info.locales.find((itemLocale) => {
              return itemLocale.inLanguage === locale;
            });
            if (findLocale == null) {
              // if item does not exist add it
              item.info.locales.push({
                inLanguage: locale
              });
            }
          }
        }
      }
      if (item.locales) {
        // get index of object with inLanguage: defaultLocale
        const removeIndex = item.locales.findIndex(x => {
          return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
        });
        // remove object
        if (removeIndex >= 0) {
          item.locales.splice(removeIndex, 1);
        }

        // map application locales
        for (const locale of this._configurationService.settings.i18n.locales) {
          if (locale !== this._configurationService.settings.i18n.defaultLocale) {
            // try to find  if localization data exist
            const findLocale = item.locales.find((itemLocale) => {
              return itemLocale.inLanguage === locale;
            });
            if (findLocale == null) {
              // if item does not exist add it
              item.locales.push({
                inLanguage: locale
              });
            }
          }
        }
      }
      return Promise.resolve(item);
    });
  }
}