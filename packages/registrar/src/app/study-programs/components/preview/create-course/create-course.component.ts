import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {LoadingService, ModalService} from '@universis/common';
import {Observable, Subscription} from 'rxjs';
import {AdvancedFormComponent} from '@universis/forms';
// tslint:disable-next-line:max-line-length

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html'
})
export class CreateCourseComponent extends RouterModalOkCancel implements AfterViewInit, OnInit, OnDestroy {

  refreshedForm = false;
  public loading = false;
  public lastError;
  @Input() execute: Observable<any>;
  @Input() items: Array<any> = [];
  @Input() data: any;
  @Input() target: any;
  @Input() beforeExecute: any;
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  formConfig: any;
  private formLoadSubscription: Subscription;
  private formChangeSubscription: Subscription;
  public hasSelected = false;

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _modalService: ModalService,
              private _loadingService: LoadingService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }
  ngAfterViewInit(): void {
    //
  }

  ngOnInit() {

    this.formLoadSubscription = this.formComponent.form.formLoad.subscribe(() => {
      if (this.formComponent.form.config) {
        // find submit button
        const findButton = this.formComponent.form.form.components.find(component => {
          return component.type === 'button' && component.key === 'submit';
        });
        // hide button
        if (findButton) {
          (<any>findButton).hidden = true;
          // refresh form
          if (!this.refreshedForm) {
            this.formComponent.form.refresh.emit({ form: this.formComponent.form.form });
            this.refreshedForm = true;
          }
        }
      }
    });
    this.formChangeSubscription = this.formComponent.form.change.subscribe((event) => {
      if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
        // enable or disable button based on form status
        // this.okButtonDisabled = (!event.isValid || !(event.data && event.data.courseType));
        this.okButtonDisabled = !event.isValid;
      }
    });
  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    // close
    if (this._modalService.modalRef) {
      return  this._modalService.modalRef.hide();
    }
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
        this.formChangeSubscription.unsubscribe();
      }
    }

  ok(): Promise<any> {
    try {
      return new Promise((resolve, reject) => {
        this.loading = false;
        this.lastError = null;
        // get form data
        const data = this.formComponent.form.formio.data;
        // set courses
        this.items.push(data);
        // execute add
        const executeSubscription = this.execute.subscribe((result) => {
          this.loading = false;
          executeSubscription.unsubscribe();
          // close modal
          if (this._modalService.modalRef) {
            this._modalService.modalRef.hide();
          }
          return resolve(null);
        }, (err) => {
          this.loading = false;
          // ensure that loading is hidden
          this._loadingService.hideLoading();
          // set last error
          this.lastError = err;
          return reject(err);
        });
      });
    } catch (err) {
      this.loading = false;
      this._loadingService.hideLoading();
      this.lastError = err;
    }
  }

  onChange($event: any) {

  }

}
