import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewTimetableComponent } from './study-programs-preview-timetable.component';

describe('StudyProgramsPreviewTimetableComponent', () => {
  let component: StudyProgramsPreviewTimetableComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewTimetableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewTimetableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewTimetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
