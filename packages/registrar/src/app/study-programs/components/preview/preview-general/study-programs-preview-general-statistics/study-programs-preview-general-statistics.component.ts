import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Observable, from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-study-programs-preview-general-statistics',
  templateUrl: './study-programs-preview-general-statistics.component.html'
})
export class StudyProgramsPreviewGeneralStatisticsComponent implements AfterViewInit {

  public studentStatus$: Observable<any>;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
    this.studentStatus$ = this._activatedRoute.params.pipe(map(params => params.id)).pipe(
      switchMap((id: any) => from(
        this._context.model('Students').select('count(id) as total', 'studentStatus/name as studentStatus', 'studentStatus/alternateName as alternateName')
          .where('studyProgram').equal(id)
          .groupBy('studentStatus/name', 'studentStatus/alternateName').getItems() as Promise<{total: number, studentStatus: string, alternateName: string}[]>
      ))
    ).pipe(switchMap((items) => 
      from(this._context.model('StudentStatuses').select('name as studentStatus', 'alternateName')
      .where('alternateName').equal('active')
      .or('alternateName').equal('removed')
      .or('alternateName').equal('graduated')
      .getItems().then((studentStatusTypes) => {
        return studentStatusTypes.map((studentStatusType: any) => {
          const item = items.find((x) => x.alternateName === studentStatusType.alternateName);
          studentStatusType.total = item ? item.total : 0;
          Object.assign(studentStatusType, { studentStatus: `StudentStatuses.${studentStatusType.alternateName}` });
          return studentStatusType;
        }) as {total: number, studentStatus: string, alternateName: string}[];
      }))
    ));
  }
  ngAfterViewInit(): void {

  }


}
