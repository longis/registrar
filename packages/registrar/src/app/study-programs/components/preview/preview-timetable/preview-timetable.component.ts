import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import {Observable, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, UserActivityService} from '@universis/common';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import { TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';

@Component({
  selector: 'app-preview-timetable',
  templateUrl: './preview-timetable.component.html',
  styleUrls: ['./preview-timetable.component.scss']
})
export class PreviewTimetableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private studyProgram: any;
  private timetable: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('timetableevents') timetableevents: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private currentTimetable: number;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              private _router: Router,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _appEvent: AppEventService,
              private resolver: TableConfigurationResolver)  { }

  async ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      // get query params
      if (this.paramSubscription) {
        this.paramSubscription.unsubscribe();
      }
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
        if (fragment === 'reload') {
          this.timetableevents.fetch();
        }
      });
      this.paramSubscription = this._activatedRoute.params.subscribe(async (params) => {

        if (data.tableConfiguration) {
          this.studyProgram = parseInt(params.id, 10) || 0;
          this.timetable = await this._context.model('TimeTableEvents')
          .where('studyPrograms/id').equal(this.studyProgram).getItem().then((item) => {
                  if (item) {return item}});      
          this.currentTimetable = this.timetable.id || 0;
          const queryParams = this._context.model('TimeTableEvents')
          .where('superEvent').equal(this.currentTimetable).getParams();
                 
          data.tableConfiguration.defaults.filter = queryParams.$filter;
          this.timetableevents.config = AdvancedTableConfiguration.cast(data.tableConfiguration, true);
          this.timetableevents.fetch(true);
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.formComponent.formLoad.subscribe((res: any) => {
            Object.assign(res, { studyProgram: this._activatedRoute.snapshot.params.id });
          });
        }
      });
    });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

}
