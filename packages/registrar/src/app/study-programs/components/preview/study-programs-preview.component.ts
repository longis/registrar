import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {AppEventService, ErrorService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-study-programs-preview',
  templateUrl: './study-programs-preview.component.html'
})
export class StudyProgramsPreviewComponent implements OnInit, OnDestroy {
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private changeSubscription: Subscription;
  public studySpecializations: any[];
  private studyProgram;
  public show: boolean;
  private toggleShowSubscription: Subscription; 

  constructor(private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              public _translateService: TranslateService,
              private _appEvent: AppEventService,
              private _templateManageService: TemplateManageService) {
    //
  }

  async ngOnInit() {
    this.toggleShowSubscription = this._templateManageService.toggleShow.subscribe((show) => this.show = show);

    this.paramSubscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgram = params.id;
      this._templateManageService.setCurrentStudyProgram(this.studyProgram);
      this.studySpecializations = await this.getSpecialties();
    });
    this.changeSubscription = this._appEvent.changed.subscribe(async (event) => {
      if (event && event.model === 'StudyProgramSpecialties') {
        this.studySpecializations = await this.getSpecialties();
      }
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        this.studySpecializations = await this.getSpecialties();
      }
    });
  }

  async getSpecialties() {
    return this._context.model('StudyProgramSpecialties')
      .where('studyProgram').equal(this.studyProgram)
      .orderBy('specialty')
      .getItems();
  }


  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
    if (this.toggleShowSubscription) {
      this.toggleShowSubscription.unsubscribe();
    }
  }

}
