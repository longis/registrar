import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
    selector: 'app-program-course-preview-form',
    templateUrl: './program-course-preview-form.component.html'
})
export class ProgramCoursePreviewFormComponent implements OnInit {

    @Input() model: any;

    constructor(private _activatedRoute: ActivatedRoute,
        private _context: AngularDataContext) {
    }

    async ngOnInit() {

    }

}
