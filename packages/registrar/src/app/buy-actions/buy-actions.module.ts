import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuyActionsRoutingModule } from './buy-actions-routing.module';
import { BuyActionsHomeComponent } from './components/buy-actions-home/buy-actions-home.component';
import { BuyActionsRootComponent } from './components/buy-actions-root/buy-actions-root.component';
import { BuyActionsTableComponent } from './components/buy-actions-table/buy-actions-table.component';
import { BuyActionsSharedModule } from './buy-actions-shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { TablesModule } from '@universis/ngx-tables';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { ElementsModule } from '../elements/elements.module';
import { MostModule } from '@themost/angular';
import { RouterModalModule } from '@universis/common/routing';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { AdvancedFormsModule } from '@universis/forms';
import { TabsModule, TooltipModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    BuyActionsSharedModule,
    BuyActionsRoutingModule,
    TranslateModule,
    TablesModule,
    SharedModule,
    FormsModule,
    ElementsModule,
    MostModule,
    RouterModalModule,
    RegistrarSharedModule,
    AdvancedFormsModule,
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
  ],
  declarations: [
    BuyActionsHomeComponent, 
    BuyActionsRootComponent, 
    BuyActionsTableComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class BuyActionsModule { }
