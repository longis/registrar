import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuyActionsHomeComponent } from './components/buy-actions-home/buy-actions-home.component';
import { BuyActionsTableComponent } from './components/buy-actions-table/buy-actions-table.component';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../registrar-shared/table-configuration.resolvers';

const routes: Routes = [
  {
      path: '',
      component: BuyActionsHomeComponent,
      data: {
          title: 'Buy actions home'
      },
      children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list/:list',
        component: BuyActionsTableComponent,
        data: {
          model: 'StudyProgramRegisterActions',
          title: 'StudyProgramRegisterActionsPayActions List'
        },
        resolve: {
          tableConfiguration: TableConfigurationResolver,
          searchConfiguration: SearchConfigurationResolver
        }, 
        // children: [
        //   {
        //     path: ':id/edit',
        //     component: EditFeeComponent,
        //     outlet: 'modal',
        //     data: <AdvancedFormModalData> {
        //       model: 'PayActions',
        //       action: 'edit',
        //       closeOnSubmit: true
        //     }
        //   }
        // ]
      }
    ]
  },
  {
   path: ':id',
      component: BuyActionsHomeComponent,
      data: {
          title: 'BuyAction Home'
      },
      children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'preview'
      },
      // {
      //   path: 'add',
      //   pathMatch: 'full',
      //   component: EditFeeComponent,
      //   outlet: 'modal',
      //   data: <AdvancedFormModalData> {
      //     model: 'StudyProgramsFees',
      //     action: 'new',
      //     closeOnSubmit: true
      //   },
      //   resolve: {
      //     formConfig: AdvancedFormResolver
      //   }
      // },
      // {
      //   path: 'edit',
      //   component: EditFeeComponent,
      //   outlet: 'modal',
      //   pathMatch: 'full',
      //   data: <AdvancedFormModalData> {
      //     model: 'StudyProgramsFees',
      //     action: 'edit',
      //     closeOnSubmit: true
      //   }
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyActionsRoutingModule { }
