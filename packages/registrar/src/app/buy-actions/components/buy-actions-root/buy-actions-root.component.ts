import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, TemplatePipe, ToastService } from '@universis/common';
import { Subscription } from 'rxjs';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';

@Component({
  selector: 'app-buy-actions-root',
  templateUrl: './buy-actions-root.component.html'
})
export class BuyActionsRootComponent implements OnInit, OnDestroy {

  public model: any;
  public studyProgramID: any;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;
  private toggleShowSubscription: Subscription;
  public show: boolean;
 
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _router: Router,
              private resolver: TableConfigurationResolver
            ) { }

  async ngOnInit() {
    // this.toggleShowSubscription = this._templateManageService.toggleShow.subscribe((show) => this.show = show);
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramID = params.id;
      this.model = await this._context.model('StudyProgramFees')
        .where('studyProgram').equal(this._activatedRoute.snapshot.params.id)
        .expand('offers')
        .getItem();
      this.resolver.get('StudyPrograms').subscribe((config) => {
        // @ts-ignore
        this.config = cloneDeep(config);
        console.log("🚀 ~ FeesRootComponent ~ this.resolver.get ~ this.config:", this.config)
        if (this.config.columns && this.model) {
          // get actions from config file
          this.actions = this.config.columns.filter(x => {
            return x.actions;
          })
            // map actions
            .map(x => x.actions)
            // get list items
            .reduce((a, b) => b, 0);
          // filter actions with student permissions
          this.allowedActions = this.actions.filter(x => {
            if (x.role) {
              if (x.role === 'action' || x.role === 'method') {
                return x;
              }
            }
          });
          this.edit = this.actions.find(x => {
            if (x.role === 'edit') {
              x.href = this._template.transform(x.href, this.model);
              return x;
            }
          });

          // // get first specialization
          // const specialization = this.model.specialties[0];
          // // if specialization exists
          // if (specialization) {
          //   const self = this;
          //   this.allowedActions.push({
          //     title: this._translateService.instant('StudyPrograms.GraduationRules'),
          //     role: 'action',
          //     href: `#/study-programs/${params.id}/preview/(modal:/specialties/${specialization.id}/rules)`
          //   });
          // }

          this.actions = this.allowedActions;
          this.actions.forEach(action => {
            if (action.href) {
              action.href = this._template.transform(action.href, this.model);
            }
            if (action.invoke) {
              action.click = () => {
                if (typeof this[action.invoke] === 'function') { this[action.invoke](); }
              };
            }
          });
        }
      });
      // this.referrerSubscription = this._referrer.routeParams$.subscribe(result => {
      //   this.referrerRoute = result || {
      //     commands: ['/study-programs']
      //   };
      // });
    });
  }

  async deleteStudyProgram() {
    try {
      const studyProgram = this.studyProgramID;
      const translations: {
        Description: string;
        ModalMessage: string;
        ModalWarningCannotDelete: {
          Students: string;
          ProgramGroups: string;
          SpecializationCourses: string
        };
        ToastMessage: string;
      }
      = this._translateService.instant('StudyPrograms.DeleteProgram');
      // in the context of the current user, try to find
      // if any students follow this study program
      this._loadingService.showLoading();
      const studentsInProgram = await
        this._context
          .model('Students')
          .where('studyProgram/id')
          .equal(studyProgram)
          .select('count(id) as total')
          .getItem();
      if (studentsInProgram && studentsInProgram.total) {
        this._loadingService.hideLoading();
        // if yes, then the delete action cannot be initiated
        return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.Students);
      }
      // try to find if the study program holds any courses
      const specializationCourses = await
        this._context
          .model('SpecializationCourses')
          .where('studyProgramCourse/studyProgram')
          .equal(studyProgram)
          .select('count(id) as total')
          .getItem();
      if (specializationCourses && specializationCourses.total) {
        this._loadingService.hideLoading();
        // if yes, then the delete action cannot be initiated
        return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.SpecializationCourses);
      }
      // try to find if the study program has any groups
      const programGroups = await
        this._context
          .model('ProgramGroups')
          .where('program')
          .equal(studyProgram)
          .select('count(id) as total')
          .getItem();
      if (programGroups && programGroups.total) {
        this._loadingService.hideLoading();
        // if yes, then the delete action cannot be initiated
        return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.ProgramGroups);
      }
      this._loadingService.hideLoading();
      // initiate a delete program action
      // first, get confirmation
      const dialogResult = await this._modalService.showWarningDialog(translations.Description,
        translations.ModalMessage,
        DIALOG_BUTTONS.OkCancel);
      if (dialogResult !== 'ok') {
        return Promise.resolve();
      }
      this._loadingService.showLoading();
      // try to delete the program
      await this._context.getService().execute({
        method: 'POST',
        url: `StudyPrograms/${studyProgram}/delete`,
        headers: {},
        data: null
      });
      this._loadingService.hideLoading();
      // and on success, show a toast message
      this._toastService.show(translations.Description, translations.ToastMessage);
      // and navigate to the programs list after a bit
      return setTimeout(() => {
        return this._router.navigate(['study-programs']);
      }, 800);
    } catch (err) {
      // show error
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      // ensure loading is hidden
      this._loadingService.hideLoading();
    }

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.toggleShowSubscription) {
      this.toggleShowSubscription.unsubscribe();
    }
  }
}
