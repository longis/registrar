import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyActionsTableComponent } from './buy-actions-table.component';

describe('BuyActionsTableComponent', () => {
  let component: BuyActionsTableComponent;
  let fixture: ComponentFixture<BuyActionsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyActionsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyActionsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
