import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvancedListComponent } from '@universis/ngx-tables';
import { AdvancedFormModalData, AdvancedFormModalComponent, AdvancedFormItemResolver } from '@universis/forms';
import { ListComponent } from './components/list/list.component';
import { EditComponent } from './components/edit/edit.component';
import { MessagesComponent } from './components/messages/messages.component';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import {ActiveDepartmentResolver} from '../registrar-shared/services/activeDepartmentService.service';

const routes: Routes = [
  {
    path: 'item/:id/edit',
    component: EditComponent,
    data: <AdvancedFormModalData>{
      model: 'StudyProgramRegisterActions',
      serviceQueryParams: EditComponent.ServiceQueryParams
    },
    resolve: {
      model: AdvancedFormItemResolver
    }
  },
  {
    path: 'messages',
    component: MessagesComponent,
    data: {
      model: 'StudyProgramRegisterActionMessages',
      description: 'Register.Messages'
    }
  },
  {
    path: 'list',
    component: ListComponent,
    data: {
      model: 'StudyProgramRegisterActions',
      description: 'Settings.Lists.StudyProgramRegisterActions.Description'
    },
    resolve: {
      department: ActiveDepartmentResolver
    },
    children: [
      {
        path: 'item/:id/edit',
        component: ModalEditComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'StudyProgramRegisterActions',
          action: 'modal-edit',
          serviceQueryParams: EditComponent.ServiceQueryParams,
          modalOptions: {
            modalClass: 'modal-dialog-100',
          },
          closeOnSubmit: true,
          continueLink: '.'
        },
        resolve: {
          model: AdvancedFormItemResolver
        }
      },
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class RegisterRoutingModule {
}
