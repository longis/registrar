import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScholarshipsPreviewComponent } from './scholarships-preview.component';

describe('ScholarshipsPreviewComponent', () => {
  let component: ScholarshipsPreviewComponent;
  let fixture: ComponentFixture<ScholarshipsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScholarshipsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScholarshipsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
