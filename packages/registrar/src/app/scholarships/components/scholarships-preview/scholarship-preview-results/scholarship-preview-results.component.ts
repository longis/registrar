import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as SCHOLARSHIP_RESULTS_LIST_CONFIG from './scholarship-results-table.config.list.json';
import {Observable, Subscription} from 'rxjs';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {RequestActionComponent} from '../../../../requests/components/request-action/request-action.component';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';


@Component({
  selector: 'app-scholarshop-preview-results',
  templateUrl: './scholarship-preview-results.component.html',
  styleUrls: ['./scholarship-preview-results.component.scss']
})
export class ScholarshipPreviewResultsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>SCHOLARSHIP_RESULTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public scholarshipId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  private selectedItems = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _errorService: ErrorService
  ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.scholarshipId = params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('StudentScholarshipValidateResults')
        .asQueryable()
        .where('action/scholarship').equal(params.id)
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(SCHOLARSHIP_RESULTS_LIST_CONFIG);
      this.students.fetch();
      // declare model for advance search filter criteria
      this.students.config.model = 'StudentScholarshipValidateResults';
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form =  Object.assign(data.searchConfiguration, { scholarship: this.scholarshipId });
          this.search.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  /***
   * Adds students to scholarship
   */
  async addStudentsAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      //Keep only students that meet the rules
      this.selectedItems = items.filter(student=>student.result===true);
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Scholarships.AddStudentsAction.Title',
          description: 'Scholarships.AddStudentsAction.Description',
          refresh: this.refreshAction,
          execute: this.executeAddStudents()
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  executeAddStudents() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              student: item.id,
              scholarship: this.scholarshipId
            };
            await this._context.model('StudentScholarships').save(updated);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.students && this.students.lastQuery) {
      const lastQuery: ClientDataQueryable = this.students.lastQuery;
      // search for document attributes (if table has published column)

      if (lastQuery != null) {
        if (this.students.smartSelect) {
          // get items
          const selectArguments = ['student/id as id','result'];

          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.students.unselected && this.students.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.students.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.students.selected.map((item) => {
            return {
              id: item.id,
              result: item.result
            };
          });
        }
      }
    }
    return items;
  }

  // Document request
   
async addDocumentRequest() {
  try {
    this._loadingService.showLoading();
    const items = await this.getSelectedItems();
    //Keep only students that meet the rules
    this.selectedItems = items.filter(student=>student.result===true);
    
    this._modalService.openModalComponent(AdvancedRowActionComponent, {
      class: 'modal-lg',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        items: this.selectedItems,
        formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addDocumentRequest' : null,
        modalTitle: 'Students.AddDocumentRequest.Title',
        description: 'Students.AddDocumentRequest.Description',
        errorMessage: 'Students.AddDocumentRequest.CompletedWithErrors',
        refresh: this.refreshAction,
        execute: this.executeAddDocumentRequest()
      }
    });
  } catch (err) {
    
    this._errorService.showError(err, {
      continueLink: '.'
    });
  } finally {
    this._loadingService.hideLoading();
  }
}


/**
 * Executes add document request for students
 */
executeAddDocumentRequest() {
  return new Observable((observer) => {
    const total = this.selectedItems.length;
    const result = {
      total: this.selectedItems.length,
      success: 0,
      errors: 0
    };
    // get values from modal component
    const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
    const data = component.formComponent.form.formio.data;
    if (data.object == null || data.object === '') {
      this.selectedItems = [];
      result.errors = result.total;
      return observer.next(result);
    }
    this.refreshAction.emit({
      progress: 1
    });

    // execute promises in series within an async method
    (async () => {
      for (let index = 0; index < this.selectedItems.length; index++) {
        try {
          const item = this.selectedItems[index];
          // load document request
          data.student = item.id;
          // set progress
          this.refreshAction.emit({
            progress: Math.floor(((index + 1) / total) * 100)
          });
          const action = await this._context.model(`RequestDocumentActions`).save(data);
          result.success += 1;
        } catch (err) {
          // log error
          console.log(err);
          result.errors += 1;
        }
      }
    })().then(() => {
      observer.next(result);
    }).catch((err) => {
      observer.error(err);
    });
  });
}

}
