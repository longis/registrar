import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesStudyProgramsComponent } from './courses-study-programs.component';

describe('CoursesStudyProgramsComponent', () => {
  let component: CoursesStudyProgramsComponent;
  let fixture: ComponentFixture<CoursesStudyProgramsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesStudyProgramsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesStudyProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
