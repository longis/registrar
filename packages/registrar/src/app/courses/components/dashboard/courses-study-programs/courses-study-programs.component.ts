import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { TemplateManageService } from '@longis/ngx-longis/registrar';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-study-programs',
  templateUrl: './courses-study-programs.component.html',
  styleUrls: ['./courses-study-programs.component.scss']
})
export class CoursesStudyProgramsComponent implements OnInit, OnDestroy {
  public model: any;
  private subscription: Subscription;
  public correctLink: string;

  constructor(readonly _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext, 
              public _templateManageService: TemplateManageService, 
              private _router: Router) { }

  async  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('ProgramCourses')
        .where('course').equal(params.id)
        .expand('course,studyProgramSpecialty,program($expand=department,studyLevel)')
        .getItems();
      this.model.forEach(programCourse => {
        this._context.model('SpecializationCourses')
          .where('studyProgramCourse').equal(programCourse.id)
          .select('id as id')
          .getItem().then((item) => {
            programCourse.specId = item.id;
          });
      });
    });
    this.correctLink = this._templateManageService.globalNavigationCorrectionService('preview');
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  navigateToUrl(specId: number) {
    this._router.navigate([`${this.correctLink} + '/(modal:item/'+ ${specId} + '/edit)'`]);
  }
}
