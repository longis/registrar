import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TemplateManageService } from '@longis/ngx-longis/registrar';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-overview-exams',
  templateUrl: './courses-overview-exams.component.html',
  styleUrls: ['./courses-overview-exams.component.scss']
})
export class CoursesOverviewExamsComponent implements OnInit, OnDestroy {
  public model: any;
  public grading: any;
  @Input() currentYear: any;
  public courseId: any;
  private subscription: Subscription;
  private currentStudyProgramSubscription: Subscription;
  private paramSubscription: Subscription;
  public studyProgram: number;
  public specialization: number;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _templateManageService: TemplateManageService) { } 

  async ngOnInit() {
    this.currentStudyProgramSubscription = this._templateManageService.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram);
    this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
      if (params && params.specialization) {
        this.specialization = params.specialization;
        this.courseId = params.id;
      }
    });
    const studyProgramEntity = await this._context.model('StudyPrograms')
                    .where('id').equal(this.studyProgram)
                    .getItem();
    if(studyProgramEntity && studyProgramEntity.currentPeriod && studyProgramEntity.currentYear && studyProgramEntity.currentPeriod.id && studyProgramEntity.currentYear.id){
    this.model = await this._context.model(`StudyPrograms/${this.studyProgram}/exams`)
        .where('course').equal(this.courseId)
        .and('year').equal(studyProgramEntity.currentYear.id)
        .and('examPeriod').equal(studyProgramEntity.currentPeriod.id)
        .expand('examPeriod,status,completedByUser,year,course($expand=department)')
        .getItems();
      }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.currentStudyProgramSubscription) {
      this.currentStudyProgramSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
