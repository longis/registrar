import {Component, Injectable, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ActivatedTableService } from '@universis/ngx-tables';
import { TemplateManageService } from '@longis/ngx-longis/registrar';
import {Args} from '@themost/client';
import {AdvancedFormParentItemResolver} from '@universis/forms';
import {Subscription, combineLatest} from 'rxjs';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CourseTitleResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any>|any {
  return  new AdvancedFormParentItemResolver(this._context).resolve(route, state).then(result => {
      return result.name;
    });
  }
}


@Component({
  selector: 'app-courses-preview-classes',
  templateUrl: './courses-preview-classes.component.html',
})
export class CoursesPreviewClassesComponent implements OnInit, OnDestroy {
  @ViewChild('classes') classes: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private subscription: Subscription;
  private currentStudyProgramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  public course: any;
  public studyProgram: number;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _resolver: TableConfigurationResolver,
    private _templateManageService: TemplateManageService,
    private _searchResolver: SearchConfigurationResolver) { }

  async ngOnInit() {
    this.subscription = combineLatest(
        this._activatedRoute.params,
        this._resolver.get('CourseClasses', 'index'),
        this._searchResolver.get('CourseClasses', 'index')
      ).pipe(
        map(([params, tableConfiguration, searchConfiguration]) => ({params, tableConfiguration, searchConfiguration}))
      ).subscribe(async (results) => {
      this.course = results.params.id;
      this._activatedTable.activeTable = this.classes;
      this.currentStudyProgramSubscription = this._templateManageService.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram);
      this.classes.query = this._context.model('courseClasses')
        .where('course').equal(results.params.id)
        .and('studyProgram/id').equal(this.studyProgram)
        .expand('course')
        .orderByDescending('year')
        .thenBy('period')
        .prepare();
      this.classes.config = AdvancedTableConfiguration.cast(results.tableConfiguration);
      this.classes.fetch();

      if (results.searchConfiguration) {
        this.search.form =  Object.assign(results.searchConfiguration, { course: this.course });
        this.search.ngOnInit();
      }

    });

    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.classes.fetch(true);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.currentStudyProgramSubscription) {
      this.currentStudyProgramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}
