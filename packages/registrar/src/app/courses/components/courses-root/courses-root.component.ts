import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {cloneDeep} from 'lodash';
import {AppEventService, ReferrerRouteParams, ReferrerRouteService, TemplatePipe, UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';
import { AdvancedTableConfiguration } from '@universis/ngx-tables';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-courses-root',
  templateUrl: './courses-root.component.html',
  providers: [TemplatePipe]
})
export class CoursesRootComponent implements OnInit, OnDestroy {

  public course: any;
  public tabs: any[];
  public actions: any[];
  public config: any;
  public isCreate = false;
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;
  private changeSubscription: Subscription;
  private currentStudyProgramSubscription: Subscription;
  private paramSubscription: Subscription;
  referrerSubscription: Subscription;
  referrerRoute: ReferrerRouteParams = {
    commands: ['']
  }

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _appEvent: AppEventService,
              private _template: TemplatePipe,
              private resolver: TableConfigurationResolver,
              private _templateManageService: TemplateManageService,
              private _referrer: ReferrerRouteService
            ) { }

  async ngOnInit() {
    this._templateManageService.updateShowStatus(false);
    this._templateManageService.updateShowRouterOutletStatus(false);
    this._templateManageService.globalNavigationCorrectionService('preview');
    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

    if (this._activatedRoute.snapshot.url.length > 0 &&
        this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }

     this.subscription = this._activatedRoute.params.subscribe(async (params) => {
         this.course = await this._context.model('Courses')
           .where('id').equal(params.id)
           .select('id', 'name', 'displayCode')
           .getItem();
       if (this.course) {
         this.setUserActivity(this.course);

         // @ts-ignore
         this.resolver.get('Courses').subscribe((config) => {
            this.config = AdvancedTableConfiguration.cast(config);
            if (this.config.columns && this.course) {
              // get actions from config file
              this.actions = this.config.columns.filter(x => {
                return x.actions;
              })
                // map actions
                .map(x => x.actions)
                // get list items
                .reduce((a, b) => b, 0);

              //TODO change
              // filter actions with student permissions
              this.allowedActions = this.actions.filter(x => {
                if (x.role) {
                  if (x.role === 'action') {
                    return x;
                  }
                }
              });
  
              this.edit = this.actions.find(x => {
                if (x.role === 'edit') {
                  x.href = '/#' + this._templateManageService.globalNavigationCorrectionService('preview') + x.href; 
                  x.href = this._template.transform(x.href, this.course);
                  return x;
                }
              });
  
              this.actions = this.allowedActions;
              this.actions.forEach(action => {
                action.href = this._template.transform(action.href, this.course);
              });
            }
         });
         
       }
     });

     this.referrerSubscription = this._referrer.routeParams$.subscribe(result => {
      this.referrerRoute = result || {
        commands: [this._templateManageService.globalNavigationCorrectionService('preview') + '/specialization-courses']
      };
    });

    this.changeSubscription = this._appEvent.change.subscribe(async change => {
      if (change && change.model === 'Courses' && change.target) {
        this.course = (({ id, name, displayCode }) => ({ id, name, displayCode }))(change.target);
        this.setUserActivity(change.target);
      }
    });
  }

  setUserActivity(course) {
    this._userActivityService.setItem({
      category: this._translateService.instant('Courses.Title'),
      description: this._translateService.instant(course.displayCode + ' - ' + course.name),
      url: window.location.hash.substring(1), // get the path after the hash
      dateCreated: new Date
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.currentStudyProgramSubscription) {
      this.currentStudyProgramSubscription.unsubscribe();
    }
    if (this.referrerSubscription) {
      this.referrerSubscription.unsubscribe();
    }
    this._templateManageService.updateShowStatus(true);
    this._templateManageService.updateShowRouterOutletStatus(false);
  }
}
