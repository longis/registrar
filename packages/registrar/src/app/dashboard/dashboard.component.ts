import { AfterViewInit, Component,  OnInit } from '@angular/core';
import {ActiveDepartmentService} from '../registrar-shared/services/activeDepartmentService.service';
import { ActivatedUser } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-theses-home',
  templateUrl: 'dashboard.component.html',
  styles: [
    `.card-total {
      min-width: 16rem;
    }`
  ]
})
export class DashboardComponent implements OnInit, AfterViewInit {
  studyPrograms$: any;
  students$: any;
  candidates$: any;

  constructor(private _activeDepartmentService: ActiveDepartmentService,
    private activatedUser: ActivatedUser,
    private translate: TranslateService,
    private context: AngularDataContext) { }
  ngAfterViewInit(): void {
    this.activatedUser.user.subscribe((user) => {
      const isRegistrar = user.groups.find((group) => group.name === 'Registrar');
      if (isRegistrar) {
        this.title = this.translate.instant('Dashboard.RegistrarModuleTitle');
      }
      const isRegistrarAssistant = user.groups.find((group) => group.name === 'RegistrarAssistants');
      if (isRegistrarAssistant) {
        this.title = this.translate.instant('Dashboard.RegistrarAssistantModuleTitle');
      }
    });
  }

  public title = 'Dashboard.ModuleTitle';
  public activeDepartment: any;
  public registrationStatus: string;

  async ngOnInit() {
    this.activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    this.registrationStatus = this.getRegistrationStatus();
    this.studyPrograms$ = from(this.context.model('StudyPrograms').select(
      'count(id) as total',
      'isActive'
    ).groupBy('isActive').orderByDescending('isActive').getItems()).pipe(map((results) => {
      return results.map((result: any) => {
        Object.assign(result, { name: result.isActive ? 'StudyPrograms.Active.Singular' : 'StudyPrograms.Inactive.Singular' })
        return result;
      });
    }));

    this.students$ = from(this.context.model('Students').select(
      'count(id) as total',
      'studentStatus/alternateName as studentStatus'
    )
    .where('studentStatus/alternateName').equal('active').or('studentStatus/alternateName').equal('graduated')
    .groupBy('studentStatus/alternateName', 'studentStatus/id').orderBy('studentStatus/id').getItems()).pipe(map((results) => {
      return results.map((result: any) => {
        Object.assign(result, { name: `StudentStatuses.${result.studentStatus}` });
        return result;
      });
    }));

    this.candidates$ = from(this.context.model('StudyProgramRegisterActions').select(
      'count(id) as total',
      'actionStatus/id as actionStatus',
      'actionStatus/alternateName as alternateName'
    ).orderBy('actionStatus/id').groupBy('actionStatus/id', 'actionStatus/alternateName').getItems()).pipe(map((results) => {
      return results.map((result: any) => {
        Object.assign(result, { name: `ActionStatusTypes.${result.alternateName}` });
        return result;
      });
    }));

  }




  /**
   *
   * Given the active department returns the registration status.
   *
   */
  getRegistrationStatus(): string {

    if (!this.activeDepartment) {
      return 'unknown';
    }

    const startingDate = new Date(this.activeDepartment.registrationPeriodStart);
    const endingDate = new Date(new Date(this.activeDepartment.registrationPeriodEnd).setHours(23, 59, 59, 59));
    const now = new Date();

    if (startingDate > now) {
      return 'pendingStart';
    } else if ( this.activeDepartment.isRegistrationPeriod && (startingDate <= now && endingDate >= now)) {
      return 'open';
    } else {
      return 'close';
    }
  }
}
