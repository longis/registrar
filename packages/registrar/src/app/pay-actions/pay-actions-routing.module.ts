import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PayActionsHomeComponent } from './components/pay-actions-home/pay-actions-home.component';
import {  PayActionsTableComponent } from './components/pay-actions-table/pay-actions-table.component';
import { TableConfigurationResolver, SearchConfigurationResolver } from '../registrar-shared/table-configuration.resolvers';
import { AdvancedFormModalData } from '@universis/forms';
import { EditFeeComponent } from './components/edit-fee/edit-fee.component';
import { StudyProgramRegisteActionPayActionsTableConfigurationResolver, StudyProgramRegisteActionPayActionsTableSearchResolver } from './study-register-actions-pay-actions-table-config.resolver';

const routes: Routes = [
  {
      path: '',
      component: PayActionsHomeComponent,
      data: {
          title: 'Pay actions home'
      },
      children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list/:list',
        component: PayActionsTableComponent,
        data: {
          model: 'StudyProgramRegisterActions',
          title: 'PayActions List'
        },
        resolve: {
          tableConfiguration: StudyProgramRegisteActionPayActionsTableConfigurationResolver,
          searchConfiguration: StudyProgramRegisteActionPayActionsTableSearchResolver
        }, 
        children: [
          {
            path: ':id/edit',
            component: EditFeeComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData> {
              model: 'PayActions',
              action: 'edit',
              closeOnSubmit: true
            }
          }
        ]
      }
    ]
  },
  {
   path: ':id',
      component: PayActionsHomeComponent,
      data: {
          title: 'PayAction Home'
      },
      children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'preview'
      },
      // {
      //   path: 'add',
      //   pathMatch: 'full',
      //   component: EditFeeComponent,
      //   outlet: 'modal',
      //   data: <AdvancedFormModalData> {
      //     model: 'StudyProgramsFees',
      //     action: 'new',
      //     closeOnSubmit: true
      //   },
      //   resolve: {
      //     formConfig: AdvancedFormResolver
      //   }
      // },
      // {
      //   path: 'edit',
      //   component: EditFeeComponent,
      //   outlet: 'modal',
      //   pathMatch: 'full',
      //   data: <AdvancedFormModalData> {
      //     model: 'StudyProgramsFees',
      //     action: 'edit',
      //     closeOnSubmit: true
      //   }
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayActionsRoutingModule { }
