import { TableConfiguration } from '@universis/ngx-tables';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { StudyProgramRegisteActionPayActions_Active } from '../pay-actions/assets/tables/StudyProgramRegisterActions/config.list.active';
import { StudyProgramRegisteActionPayActions_All } from '../pay-actions/assets/tables/StudyProgramRegisterActions/config.list.all';
import { StudyProgramRegisteActionPayActions_Search_Active } from '../pay-actions/assets/tables/StudyProgramRegisterActions/search.active';
import { StudyProgramRegisteActionPayActions_Search_All } from '../pay-actions/assets/tables/StudyProgramRegisterActions/search.all';



export class  StudyProgramRegisteActionPayActionsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'active') {
                return StudyProgramRegisteActionPayActions_Active;
            }
            else if (route.params.list == 'all') {
                return StudyProgramRegisteActionPayActions_All;
            }
        } catch (err) {
            console.log(err);
            return StudyProgramRegisteActionPayActions_All;
        }
    }
}

export class StudyProgramRegisteActionPayActionsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'active') {
                return StudyProgramRegisteActionPayActions_Search_Active;
            }
            else if (route.params.list == 'results') {
                return StudyProgramRegisteActionPayActions_Search_All;
            }
        } catch (err) {
            console.log(err);
            return StudyProgramRegisteActionPayActions_Search_All;
        }
    }
}
