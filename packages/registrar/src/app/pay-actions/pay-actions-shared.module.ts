import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';
import { RouterModalModule } from '@universis/common/routing';
import { AdvancedFormsModule } from '@universis/forms';
import { environment } from '../../environments/environment';

import * as en from './i18n/payActions.en.json';
import * as el from './i18n/payActions.el.json';

const translations = {
  en,
  el
};

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    MostModule,
    RouterModalModule,
    AdvancedFormsModule,
  ],
  declarations: [
  ]
})

export class PayActionsSharedModule { 
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PayActionsSharedModule,
      providers: [
      ]
    };
  }

  constructor(private _translateService: TranslateService) {
    
    environment.languages.forEach((language: string) => {
      if (Object.prototype.hasOwnProperty.call(translations, language)) {
        this._translateService.setTranslation(language, translations[language], true);
      }
    });

  }

}
