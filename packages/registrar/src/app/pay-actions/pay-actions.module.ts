import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';
import { RouterModalModule } from '@universis/common/routing';
import { AdvancedFormsModule } from '@universis/forms';
import { TablesModule } from '@universis/ngx-tables';
import {  TabsModule, TooltipModule } from 'ngx-bootstrap';
import { ElementsModule } from '../elements/elements.module';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { EditFeeComponent } from './components/edit-fee/edit-fee.component';
import { PayActionsSharedModule } from './pay-actions-shared.module';
import { PayActionsRoutingModule } from './pay-actions-routing.module';
import { PayActionsHomeComponent } from './components/pay-actions-home/pay-actions-home.component';
import { PayActionsRootComponent } from './components/pay-actions-root/pay-actions-root.component';
import { PayActionsTableComponent } from './components/pay-actions-table/pay-actions-table.component';
import { StudyProgramRegisteActionPayActionsTableConfigurationResolver, StudyProgramRegisteActionPayActionsTableSearchResolver } from './study-register-actions-pay-actions-table-config.resolver';

@NgModule({
  imports: [
    CommonModule,
    PayActionsSharedModule,
    PayActionsRoutingModule,
    TranslateModule,
    TablesModule,
    SharedModule,
    FormsModule,
    ElementsModule,
    MostModule,
    RouterModalModule,
    RegistrarSharedModule,
    AdvancedFormsModule,
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
  ],
  declarations: [
    PayActionsHomeComponent,
    PayActionsRootComponent,
    PayActionsTableComponent,
    EditFeeComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    StudyProgramRegisteActionPayActionsTableConfigurationResolver,
    StudyProgramRegisteActionPayActionsTableSearchResolver
  ]
})
export class PayActionsModule { }
