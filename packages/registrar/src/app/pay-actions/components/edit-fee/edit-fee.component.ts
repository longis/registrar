import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService, ToastService, ErrorService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-fee',
  templateUrl: './edit-fee.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
    .modal-dialog {
        position: relative;
        max-width: 50%;
    }

    .inner__content {
        max-height: 70vh;
        overflow-y: auto;
    }

    @media (max-width: 1200px) {
        .inner__content {
            max-height: unset;
            overflow: hidden;
        }
      
        .modal-dialog {
            position: relative;
            max-width: 100%;
        }
    }
    `
  ]
})
export class EditFeeComponent extends RouterModalOkCancel implements OnInit, OnDestroy, AfterViewInit {

  @Output() modelChange: EventEmitter<any> = new EventEmitter();

  dataSubscription: Subscription;
  paramSubscription: Subscription;
  @ViewChild('form') form?: AdvancedFormComponent;
  public formConfig: string;
  public model: any;
  public studyProgram: any;
  public isLoading: boolean = true;
  public payActions: any;

  constructor(protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _translate: TranslateService,
    private _loading: LoadingService,
    private _toastService: ToastService,
    private _errorService: ErrorService) {
    super(_router, _activatedRoute);
    this.modalTitle = this._translate.instant('PayActions.Modal.EditFee');
  }

  ngOnInit(): void {
    this._loading.showLoading();
    this.isLoading = true;
    this.paramSubscription = this._activatedRoute.params.subscribe(async params => {
      if (params && params.hasOwnProperty('id')) {
        this.studyProgram = params['id'];
        console.log(this.studyProgram)
        await this._context.model('StudyPrograms')
          .where('id')
          .equal(this.studyProgram)
          .expand('price($expand=offers)')
          .getItem().then(res => {
              this.model = res;
              console.log("🚀 ~ EditFeeComponent ~ .getItem ~ this.model:", this.model)
              this.formConfig = this.model && this.model.price != null ? 'StudyProgramFees/edit' : 'StudyProgramFees/new';
          }).catch(err => {
            console.log(err);
            return this._errorService.navigateToError(err);
          });
      } 
      this._loading.hideLoading();
      this.isLoading = false;
    });
  }

  ngAfterViewInit() {

  }

  ok(): Promise<any> {
    return this._context.model(`StudyProgramFees`).save(this.model.price)
      .then(() => {
        // show success toast message
        this._toastService.show(
          this._translate.instant('iTheses.ThesisProposals.EditThesis.SuccessTitle'),
          this._translate.instant('iTheses.ThesisProposals.EditThesis.SuccessMessage')
        );
        // close modal
        this.close({
          fragment: 'reload',
          skipLocationChange: true
        });
      }).catch(err => {
        // show fail toast message
        this._toastService.show(
          this._translate.instant('iTheses.ThesisProposals.EditThesis.FailTitle'),
          this._translate.instant('iTheses.ThesisProposals.EditThesis.FailMessage')
        );
        // close modal without triggering a reload
        this.cancel();
      });
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }

  }
}


