import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-pay-actions-home',
  templateUrl: './pay-actions-home.component.html'
})
export class PayActionsHomeComponent implements OnInit, OnDestroy {

  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  public tabs: Array<any> = [];
  private paramSubscription: Subscription;

  constructor(private resolver: TableConfigurationResolver) { }

  ngOnInit() {

    this.resolver.get('PayActions').subscribe((config: any) => {
      this.paths = config.paths;
      this.activePaths = this.paths.filter(x => {
        return x.show === true;
      }).slice(0);
    });

  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex(x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

}
