import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationsPreviewComponent } from './registrations-preview.component';

describe('RegistrationsPreviewComponent', () => {
  let component: RegistrationsPreviewComponent;
  let fixture: ComponentFixture<RegistrationsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
