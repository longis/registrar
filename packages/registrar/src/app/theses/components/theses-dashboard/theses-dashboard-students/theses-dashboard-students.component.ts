import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '@universis/ngx-tables';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {Subscription, combineLatest} from 'rxjs';
import {ActivatedTableService} from '@universis/ngx-tables';
import { map } from 'rxjs/operators';
import { TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';

@Component({
  selector: 'app-theses-dashboard-students',
  templateUrl: './theses-dashboard-students.component.html',
})
export class ThesesDashboardStudentsComponent implements OnInit, OnDestroy {

  public model: any;
  private dataSubscription: Subscription;
  private subscription: Subscription;
  @ViewChild('students') students: AdvancedTableComponent;
  public thesesID: any;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _appEvent: AppEventService,
              private _context: AngularDataContext,
              private _resolver: TableConfigurationResolver) {}

  async ngOnInit() {

    this.subscription = combineLatest(
      this._activatedRoute.params, this._resolver.get('StudentTheses', 'students')
      ).pipe(
        map(([params, tableConfiguration]) => ({params, tableConfiguration}))
      ).subscribe(async (results) => {
      this._activatedTable.activeTable = this.students;
      this.thesesID = results.params.id;
      this.students.query = this._context.model('Theses/' + this.thesesID + '/students')
        .asQueryable()
        .expand('student($expand=department,person,studentStatus)')
        .prepare();

      this.students.config = AdvancedTableConfiguration.cast(results.tableConfiguration);
      this.students.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe( fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });
      // this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      //   if (data.tableConfiguration) {
      //     this.students.config = data.tableConfiguration;
      //     this.students.ngOnInit();
      //   }
      // });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.students && this.students.selected && this.students.selected.length) {

      const items = this.students.selected.filter( x => {
        return x.studentStatus === 'active';
      }).map( student => {
       return {
         thesis: this.thesesID,
         student: student
       };
      });

      if (items && items.length > 0) {
        return this._modalService.showWarningDialog(
          this._translateService.instant('Theses.RemoveStudentTitle'),
          this._translateService.instant('Theses.RemoveStudentMessage'),
          DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._loadingService.showLoading();
            this._context.model('StudentTheses').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Theses.RemoveStudentsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                    'Theses.RemoveStudentsMessage.one' : 'Theses.RemoveStudentsMessage.many')
                  , {value: items.length})
              );
              this.students.fetch(true);
              this._appEvent.change.next({
                model: 'Theses',
                target: this.thesesID
              });
              this._loadingService.hideLoading();
            }).catch(err => {
              this._loadingService.hideLoading();
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
      } else {
        this._modalService.showErrorDialog(this._translateService.instant('Theses.DeleteAction.CannotDelete.Title'),
             this._translateService.instant('Theses.DeleteAction.CannotDelete.Student'));
      }
    }
  }
}

