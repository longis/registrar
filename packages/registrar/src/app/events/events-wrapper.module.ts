import { EventsModule } from '@universis/ngx-events';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [EventsModule]
})
export class EventsWrapperModule {
}
