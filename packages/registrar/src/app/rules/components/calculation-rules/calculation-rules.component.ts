import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {RuleService} from '../../services/rule.service';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {OnDestroy} from '@angular/core/src/metadata/lifecycle_hooks';
import {Subscription} from 'rxjs/Subscription';
import {AdvancedFormComponent, ServiceUrlPreProcessor} from '@universis/forms';
import {AngularDataContext} from '@themost/angular';
import * as CourseTypeCalculationRuleForm from '../../assets/forms/CourseTypeCalculationRule.json';
import * as ThesisCalculationRuleForm from '../../assets/forms/ThesisCalculationRule.json';
import * as SemesterCalculationRuleForm from '../../assets/forms/SemesterCalculationRule.json';

import {ErrorService, ModalService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {DataModelRule} from '../..';
import * as NotSupportedRule from '../../assets/forms/NotSupportedRule.json';



@Component({
  selector: 'app-calculation-rules',
  templateUrl: './calculation-rules.component.html',
  styleUrls: ['./calculation-rules.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.None
})
export class CalculationRulesComponent implements OnInit, OnChanges {

  components: Map<string, any> = new Map([
    ['CourseTypeCalculationRule', CourseTypeCalculationRuleForm],
    ['StudentThesisCalculationRule', ThesisCalculationRuleForm]
  ]);


  constructor(private _rules: RuleService,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _injector: Injector,
              private _modalService: ModalService,
              private _translateService: TranslateService) {
    //
  }

  @Input() items: {
    form: any;
    data: any;
  }[] = [];

  public configuration: DataModelRule;

  @Input() target: any;
  @Input() entitySet: string;
  @Input() navigationProperty: string;

  ngOnInit() {

  }

  copyForm(srcForm: any): any {
    if (srcForm == null) {
      return null;
    }
    return JSON.parse(JSON.stringify(srcForm));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (Object.prototype.hasOwnProperty.call(changes, 'target')) {
      if (changes.target != null) {
        // get rules
        this._context.model('CalculationRules').where('studyProgram').equal(this.target.id)
          .expand('ruleType').orderBy('id').getItems().then((results) => {
          // get available children
          this.configuration = this._rules.getConfiguration(this.entitySet, this.navigationProperty);
          this.items = results.map((result) => {
            let formConfig = this.components.get(`${result.ruleType.alternateName}CalculationRule`);
            if (formConfig == null) {
              formConfig = NotSupportedRule;
            }
            // copy form
            const formCopy = this.copyForm(formConfig);
            // run pre-processors
            new ServiceUrlPreProcessor(this._context).parse(formCopy);
            result = Object.assign(result, {'studyProgram': this.target});
            return {
              form: formCopy,
              data: result
            };
          });
        });
      } else {
        this.items = [];
      }
    }
  }

  save(): Promise<any> {
    try {
      const items = this.items.map((item) => {
        if ((<any>(item.data)).remove) {
          // set state property
          Object.assign(item.data, {
            $state: 4
          });
        }
        if (item.data.ruleType.alternateName === 'StudentThesis' && item.data.checkValues === '') {
          item.data.checkValues = null;
        }
        return item.data;
      });
      // validate items
      const courseTypeItems = items.filter(x => {
        return x.ruleType.alternateName === 'CourseType' && x.$state !== 4;
      });
      const allTypesExist = courseTypeItems.find(x => {
        return x.checkValues == '-1';
      });
      if (courseTypeItems.length > 1 && allTypesExist) {
        // cannot save items
        this._modalService.showErrorDialog(this._translateService.instant('Rules.CourseTypeCalculationRule.Title'),
          this._translateService.instant('Rules.CourseTypeCalculationRule.CourseTypesError')).then(r => {
        });

      } else {
        // check also thesis types
        const thesisItems = items.filter(x => {
          return x.ruleType.alternateName === 'StudentThesis' && x.$state !== 4;
        });
        const thesisAll = thesisItems.find(x => {
          return x.checkValues === null;
        });
        if (thesisItems.length > 1 && thesisAll) {
          // cannot save items
          this._modalService.showErrorDialog(this._translateService.instant('Rules.StudentThesisCalculationRule.Title'),
            this._translateService.instant('Rules.StudentThesisCalculationRule.ThesisTypesError')).then(r => {
          });
        } else {
          return this._context.model('CalculationRules').save(items);
        }
      }
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  add(event: MouseEvent, componentType: string) {
    event.preventDefault();
    // copy form
    const formCopy = this.copyForm(this.components.get(componentType));
    // run pre-processors
    new ServiceUrlPreProcessor(this._context).parse(formCopy);
    const newItem = {
      form: formCopy,
      data: <any>{
        studyProgram: this.target

      }
    };
    this.items.push(newItem);
  }

  onCustomEvent(event, advancedForm: AdvancedFormComponent, index: number) {
    if (event.type === 'remove') {
      const findItem = this.items[index];
      if (findItem) {
        // if item is going to be removed
        if (findItem.data.id) {
          // update collection item
          Object.assign(findItem.data, event.data, {
            $state: 4 // and set state for delete
          });
        } else {
          // else if item is new remove it from collection
          this.items.splice(index, 1);
        }
      }
    }
  }

}

@Component({
  selector: 'app-calculation-rules-modal',
  template: `
    <app-calculation-rules #itemRules></app-calculation-rules>
  `,
})
export class CalculationRulesModalComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  @ViewChild('itemRules') itemRulesComponent: CalculationRulesComponent;
  private subscription: Subscription;


  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _translateService: TranslateService) {
    super(router, activatedRoute);
    this.modalClass = 'modal-xl';
    this.modalTitle = 'Rules.Title';
  }

  cancel(): Promise<any> {
    return super.close();
  }

  ngOnInit(): void {
    //
    this.subscription = this.activatedRoute.data.subscribe((routeData) => {
      if (routeData.model == null) {
        throw new Error('Item model cannot be empty at this context.');
      }
      this._context.getMetadata().then((metadata) => {
        const findEntitySet = metadata.EntityContainer.EntitySet.find((item) => {
          return item.Name === routeData.model;
        });
        if (findEntitySet == null) {
          throw new Error('Entity type cannot be found.');
        }
        // set model title from entitySet
        this.modalTitle = this._translateService.instant(`Rules.RuleTypes.${routeData.navigationProperty}`);
        // set target navigation property
        this.itemRulesComponent.navigationProperty = routeData.navigationProperty;
        // set target type
        this.itemRulesComponent.entitySet = findEntitySet.Name;
        // set data
        const data = routeData.data;
        if (Object.prototype.hasOwnProperty.call(routeData, 'department')) {
          Object.assign(data, {'department': routeData.department});
        }
        this.itemRulesComponent.target = data;
        this.itemRulesComponent.ngOnChanges({
          target: data
        });
      });
    });
  }


  ok(): Promise<any> {
    // validate data
    return this.itemRulesComponent.save().then(() => {
      return super.close();
    }).catch((err) => {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
