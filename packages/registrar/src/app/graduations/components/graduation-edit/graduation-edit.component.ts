import {Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RouterModalPreviousNextCancel} from '@universis/common/routing';
import {combineLatest, forkJoin, Subscription} from 'rxjs';
import {BsLocaleService, TabsetComponent} from 'ngx-bootstrap';
import {ConfigurationService, ErrorService, LoadingService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Form, NgForm} from '@angular/forms';
import {AngularDataContext} from '@themost/angular';
import {DatePipe} from '@angular/common';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';
import {ResponseError} from '@themost/client';

@Component({
  selector: 'app-graduation-edit',
  templateUrl: './graduation-edit.component.html',
  styles: [`
      .tab-wizard .nav {
          display: none;
        }
      .tab-wizard .tab-content {
          border-top: none !important;
        }
      .simple-check-list {
        height: 164px;
        overflow-y: auto;
      }
      .form-required::after {
        content: "(*)"
      }
      .form-control[readonly] {
        background-color: #fff;
      }
    `
  ],
  encapsulation: ViewEncapsulation.None
})
export class GraduationEditComponent extends RouterModalPreviousNextCancel  implements OnInit, OnDestroy {

  public model: any = {};
  @Input('department') department: any;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  private paramsSubscription: Subscription;
  public currentTab = 0;
  @ViewChild('form1') form1: NgForm;
  @ViewChild('form2') form2: NgForm;
  @ViewChild('form3') form3: NgForm;
  @ViewChild('form4') form4: NgForm;
  public activeForm: NgForm;
  public lastError: any;
  private formStatusSubscription: Subscription;
  public reportTemplates: Array<any> = [];
  public attachmentTypes: Array<any> = [];
  public studyPrograms: Array<any> = [];
  public selectAllStudyPrograms = false;
  private documentConfigurations: Array<any> = [];

  compareSelectOption(a, b) {
    // tslint:disable-next-line:triple-equals
    return (a == b) || (a && b && (a.id == b.id || a == b.id || a.id == b));
  }

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _configurationService: ConfigurationService,
              private _localeService: BsLocaleService,
              private _translateService: TranslateService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    // set modal title
    this.modalTitle = 'Graduations.EditGraduation';
  }

  cancel(): Promise<any> {
    return this.close();
  }

  setActiveForm(current: number) {
    // set active form
    switch (current) {
      case 0: this.activeForm = this.form1; break;
      case 1: this.activeForm = this.form2; break;
      case 2: this.activeForm = this.form3; break;
      case 3: this.activeForm = this.form4; break;
    }
    if (this.activeForm) {
      if (this.formStatusSubscription) {
        this.formStatusSubscription.unsubscribe();
      }
      this.formStatusSubscription = this.activeForm.statusChanges.subscribe(() => {
        // validate form status
        this.nextButtonDisabled = this.activeForm.invalid;
      });
      // initial state
      this.nextButtonDisabled = this.activeForm.invalid;
    }
  }

  async previous(): Promise<any> {
    // clear error
    this.lastError = null;
    if (this.currentTab > 0) {
      this.currentTab -= 1;
      this.staticTabs.tabs[this.currentTab].active = true;
      this.previousButtonDisabled = (this.currentTab === 0);
      this.nextButtonText = this._translateService.instant('Forms.Next') + ' >';
      this.setActiveForm(this.currentTab);
      return;
    }
  }

  async next(): Promise<any> {
    // clear error
    this.lastError = null;
    if (this.currentTab < this.staticTabs.tabs.length - 1) {
      this.currentTab += 1;
      this.staticTabs.tabs[this.currentTab].active = true;
      this.previousButtonDisabled = false;
      if (this.currentTab === this.staticTabs.tabs.length - 1) {
        this.nextButtonText = this._translateService.instant('Forms.Submit');
      }
      this.setActiveForm(this.currentTab);
      return;
    }
    // this is the last tab so submit form
    if (this.currentTab === this.staticTabs.tabs.length - 1) {
      const result = await this.submit();
      if (result) {
        return this.close({
          fragment: 'reload',
          skipLocationChange: true
        });
      }
    }
  }

  timeToDate(time) {
    const match = /^(\d+):(\d+)(:(\d+))?$/.exec(time);
    if (match) {
        const res = new Date();
        res.setHours(parseInt(match[1], 10));
        res.setMinutes(parseInt(match[2], 10));
        res.setSeconds(0);
        if (match[4]) {
          res.setSeconds(parseInt(match[4], 10));
        }
        return res;
    }
    throw new Error('Invalid time format. Expected HH:mm(:ss).');
  }

  /**
   * Sync selected study programs before submit
   */
  private beforeSubmitStudyPrograms() {
    // get selected items
    const selectedItems = this.studyPrograms.filter((x) => x.selected);
    this.model.studyPrograms = this.model.studyPrograms || [];
    // try to remove items (set item state to be removed)
    this.model.studyPrograms.forEach((studyProgram) => {
      const selectedIndex = selectedItems.findIndex((x) => x.id === studyProgram.id);
      if (selectedIndex < 0) {
        // set state to delete
        Object.defineProperty(studyProgram, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
      }
    });
    selectedItems.forEach((studyProgram) => {
      const findIndex = this.model.studyPrograms.findIndex((x) => x.id === studyProgram.id);
      if (findIndex < 0) {
        // add item
        this.model.studyPrograms.push(studyProgram);
      }
    });
  }

  /**
   * Sync selected attachment types before submit
   */
  private beforeSubmitAttachmentTypes() {
    // get selected items
    const selectedItems = this.attachmentTypes.filter((x) => x.selected);
    this.model.attachmentTypes = this.model.attachmentTypes || [];
    // try to remove items (set item state to be removed)
    this.model.attachmentTypes.forEach((attachmentConfiguration) => {
      const selectedIndex = selectedItems.findIndex((x) => x.id === attachmentConfiguration.attachmentType);
      if (selectedIndex < 0) {
        // set state to delete
        Object.defineProperty(attachmentConfiguration, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
      }
    });
    selectedItems.forEach((attachmentType) => {
      const findIndex = this.model.attachmentTypes.findIndex((x) => x.attachmentType === attachmentType.id);
      if (findIndex < 0) {
        // add item
        this.model.attachmentTypes.push({
          attachmentType: attachmentType
        });
      }
    });
  }

  /**
   * Sync selected report templates before submit
   */
  private beforeSubmitReportTemplates() {
    // set selected study programs
    const selectedItems = this.reportTemplates.filter((x) => x.selected);
    this.model.reportTemplates = this.model.reportTemplates || [];
    // try to remove items (set item state to be removed)
    this.model.reportTemplates.forEach((reportTemplate) => {
      const selectedIndex = selectedItems.findIndex((x) => x.id === reportTemplate.id);
      if (selectedIndex < 0) {
        // set state to delete
        Object.defineProperty(reportTemplate, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
      }
    });
    selectedItems.forEach((reportTemplate) => {
      const findIndex = this.model.reportTemplates.findIndex((x) => x.id === reportTemplate.id);
      if (findIndex < 0) {
        // add item
        this.model.reportTemplates.push(reportTemplate);
      }
    });
  }

  async submit() {
    try {
      this._loadingService.showLoading();
      // check start date
      if (this.model.startDate) {
        // set event time
        if (this.model.startTime) {
          // convert start time to date
          const time = this.timeToDate(this.model.startTime);
          // add hours and minutes
          this.model.startDate.setHours(time.getHours());
          this.model.startDate.setMinutes(time.getMinutes());
        }
        // compare dates
        if (this.model.validThrough && this.model.validFrom) {
          if (this.model.startDate < this.model.validThrough || this.model.validThrough < this.model.validFrom) {
            throw new Error(this._translateService.instant('Graduations.CheckDates'));
          }
        } else {
          if (this.model.validThrough || this.model.validFrom) {
            throw new Error(this._translateService.instant('Graduations.CheckDates'));
          }
        }
      } else {
        throw new Error(this._translateService.instant('Graduations.GraduationDateRequired'));
      }
      // sync attachment types
      this.beforeSubmitAttachmentTypes();
      // sync report templates
      this.beforeSubmitReportTemplates();
      // sync study programs
      this.beforeSubmitStudyPrograms();
      // and finally submit
      await this._context.model('GraduationEvents').save(this.model);
      this._loadingService.hideLoading();
      return true;
    } catch (err) {
      this._loadingService.hideLoading();
      this.lastError = err;
      return false;
    }
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.formStatusSubscription) {
      this.formStatusSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.nextButtonDisabled = false;
    this.setActiveForm(this.currentTab);
    this.nextButtonText = this._translateService.instant('Forms.Next') + ' >';
    this.previousButtonDisabled = true;
    this.previousButtonText = '< ' + this._translateService.instant('Forms.Previous');

    this._loadingService.showLoading();
    this.paramsSubscription = combineLatest([
      this.activatedRoute.data,
      this.activatedRoute.params
    ]).subscribe((data) => {
      const routeData = data[0];
      const routeParams = data[1];
      // get action
      if (routeData.action === 'new') {
        // set modal title
        this.modalTitle = 'Graduations.NewGraduation';
      }
      // get active department
      this._activeDepartmentService.getActiveDepartment().then((activeDepartment) => {
        // get academic years, periods
        const sources = [
          this._context.model('AttachmentTypes').asQueryable()
              .take(-1)
              .orderBy('name')
              .getItems(),
          this._context.model('ReportTemplates')
              .where('reportCategory/appliesTo').equal('Student')
              .take(-1)
              .orderBy('name')
              .getItems(),
          this._context.model('StudyPrograms')
              .where('department').equal(activeDepartment.id)
              .select('id', 'name', 'studyLevel/name as studyLevel', 'currentYear/alternateName as currentYear', 'currentPeriod/locale/name as currentPeriodName', 'currentPeriod/alternateName as currentPeriod')
              .take(-1)
              .orderBy('name')
              .getItems(),
          this._context.model('DocumentConfigurations')
            .where('reportTemplate').notEqual(null)
            .select('reportTemplate')
            .groupBy('reportTemplate')
            .take(-1)
            .getItems()
        ];
        if (routeParams.id) {
          sources.push(
              this._context.model('GraduationEvents')
                  .where('id').equal(routeParams.id)
                  .expand('location,studyPrograms,attachmentTypes,reportTemplates')
                  .getItem()
          );
        } else {
          sources.push(Promise.resolve({
            organizer: activeDepartment,
            graduationYear: activeDepartment && activeDepartment.currentYear,
            graduationPeriod: activeDepartment && activeDepartment.currentPeriod
          }));
        }
        // get data
        return Promise.all(sources).then((results) => {
          this.attachmentTypes = results[0];
          this.reportTemplates = results[1];
          this.studyPrograms = results[2];
          this.documentConfigurations = results[3];
          this.model = results[4];
          if (this.model == null) {
            throw new ResponseError('Not Found', 404);
          }
          // format startTime
          if (this.model && this.model.startDate instanceof Date) {
            const startDate: Date = this.model.startDate;
            const pipe = new DatePipe(this._configurationService.currentLocale);
            this.model.startTime = pipe.transform(startDate, 'HH:mm');
          }
          // select study programs
          if (Array.isArray(this.model.studyPrograms)) {
            this.studyPrograms.forEach((studyProgram) => {
              const findIndex = this.model.studyPrograms.findIndex((x) => x.id === studyProgram.id);
              studyProgram.selected = findIndex >= 0;
            });
          }
          // select attachment types
          if (Array.isArray(this.model.attachmentTypes)) {
            this.attachmentTypes.forEach((attachmentType) => {
              const findIndex = this.model.attachmentTypes.findIndex((x) => x.attachmentType === attachmentType.id);
              attachmentType.selected = findIndex >= 0;
            });
          }
          // filter only report templates that exists at documentConfigurations
          this.reportTemplates = this.reportTemplates.filter(x => {
            return this.documentConfigurations.findIndex(y => {
              return y.reportTemplate === x.id;
            }) >= 0;
          });
          // select report templates
          if (Array.isArray(this.model.reportTemplates)) {
            this.reportTemplates.forEach((reportTemplate) => {
              const findIndex = this.model.reportTemplates.findIndex((x) => x.id === reportTemplate.id);
              reportTemplate.selected = findIndex >= 0;
            });
          }
          this._loadingService.hideLoading();
        });
      }).catch((err) => {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    });
  }


  onSelectAllStudyPrograms(event: any) {
    this.studyPrograms.forEach( (studyProgram) => {
      studyProgram.selected = this.selectAllStudyPrograms;
    });
  }

  onStudyProgramSelected(studyProgram: any) {
    if (studyProgram.selected === false) {
      this.selectAllStudyPrograms = false;
    }
  }

  clearDates() {
    this.model.validFrom = null;
    this.model.validThrough = null;
  }
}
