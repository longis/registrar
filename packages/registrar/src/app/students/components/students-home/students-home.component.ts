import {AfterViewInit, Component, OnDestroy, OnInit, Optional} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Observable, ReplaySubject, Subscription} from 'rxjs';
import { ActivatedTableService, AdvancedTableComponent } from '@universis/ngx-tables';
import { ActiveTableQueryBuilder } from '../../../query-builder/active-table-query-builder';
import { QueryProperty } from '@universis/ngx-query-builder';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import * as fromBuilder from '../students-table/students-query-builder';
import { UserQuery } from '../../../query-builder/model';
import { ApplicationDatabase } from '../../../registrar-shared/services/app-db.service';
import { AppEventService } from '@universis/common';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-students-home',
  templateUrl: './students-home.component.html',
  styleUrls: ['./students-home.component.scss'],
  providers: [
    ActiveTableQueryBuilder
  ]
})
export class StudentsHomeComponent implements OnInit, AfterViewInit, OnDestroy {

  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  public table: AdvancedTableComponent;
  public tabs: Array<any> = [];
  private paramSubscription: Subscription;
  public readonly includeProperties = fromBuilder.properties.map((item) => item.name);

  public query: any = {
    type: 'LogicalExpression',
    operator: '$and',
    elements: [
      {
        type: 'BinaryExpression',
        operator: '$eq',
        left: null,
        right: null
      }
    ]
  };

  private queryCollectionSource = new ReplaySubject<UserQuery[]>(1);
  public queryCollection$: Observable<UserQuery[]> = this.queryCollectionSource.asObservable();
  routeSubscription: Subscription;
  private changeSubscription: Subscription;
  private removeSubscription: Subscription;
  customParams: any = {};

  constructor(private activatedRoute: ActivatedRoute,
    public activatedTable: ActivatedTableService,
    public activeQuery: ActiveTableQueryBuilder,
    private context: AngularDataContext,
    private activeDepartment: ActiveDepartmentService,
    private translateService: TranslateService,
    private applicationDatabase: ApplicationDatabase,
    private appEvent: AppEventService,
    private resolver: TableConfigurationResolver) {
    }
  ngAfterViewInit(): void {

    this.applicationDatabase.db.table('UserQuery').where('entityType').equals('Student').toArray().then((items) => {
      this.queryCollectionSource.next(items);
    });

    this.changeSubscription = this.appEvent.changed.subscribe((event: { model: string; target: UserQuery }) => {
      if (event.model === 'UserQuery') {
        this.applicationDatabase.db.table('UserQuery').where('entityType').equals('Student').toArray().then((items) => {
          this.queryCollectionSource.next(items);
        });
      }
    });

    this.removeSubscription = this.appEvent.removed.subscribe((event: { model: string; target: UserQuery }) => {
      if (event.model === 'UserQuery') {
        this.applicationDatabase.db.table('UserQuery').where('entityType').equals('Student').toArray().then((items) => {
          this.queryCollectionSource.next(items);
        });
      }
    });

    this.table = this.activatedTable.activeTable;
    this.routeSubscription = this.activatedRoute.queryParams.subscribe((queryParams) => {
      this.activeQuery.find(queryParams).subscribe((item) => {
        if (item) {
          this.activeQuery.query = item.query;
        }
      });
    });
  }

  ngOnInit() {
    this.resolver.get('Students').subscribe((config) => {
      this.paths = (config as any).paths;
      this.activePaths = this.paths.filter( x => {
        return x.show === true;
      }).slice(0);
      this.paramSubscription = this.activatedRoute.firstChild.params.subscribe( params => {
        const matchPath = new RegExp('^list/' + params.list  + '$', 'ig');
        const findItem = this.paths.find( x => {
          return matchPath.test(x.alternateName);
        });
        if (findItem) {
          this.showTab(findItem);
        }
      });
      this.activeDepartment.getActiveDepartment().then((item) => {
        Object.assign(this.customParams, {
          currentDepartment: item && item.id
        });
      });
    });
  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex( x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.changeSubscription) { this.changeSubscription.unsubscribe(); }
    if (this.removeSubscription) { this.removeSubscription.unsubscribe(); }
  }

  onLoadingProperties(properties: QueryProperty[]) {
    properties.forEach((property) => {
      const find = fromBuilder.properties.find((item) => '$'.concat(item.name) === property.value);
      if (find) {
        Object.assign(property, {
          label: this.translateService.instant(find.label),
        });
        if (find.source) {
          Object.assign(property, {
            source: find.source
          });
        }
        if (find.template) {
          Object.assign(property, {
            template: find.template
          });
        }
      }
    });
    // add missing properties
    fromBuilder.properties.forEach((property) => {
      const find = properties.find((item) => '$'.concat(property.name) === item.value);
      if (find == null) {
        properties.push({
          label: this.translateService.instant(property.label),
          value: `$${property.name}`,
          type: property.type,
          source: property.source,
          template: property.template
        });
      }
    });

    const departmentSnapshot = properties.find((property) => property.value === '$departmentSnapshot');
    if (departmentSnapshot) {
      departmentSnapshot.value = '$departmentSnapshot/id';
    }

  }


}
