import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import { ActivatedTableService, AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import * as STUDENTS_STUDY_PROFILES_LIST_CONFIG from './students-study-profiles.config.list.json';

@Component({
  selector: 'app-students-study-profiles',
  templateUrl: './students-study-profiles.component.html'
})
export class StudentsStudyProfilesComponent implements OnInit, OnDestroy {
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>STUDENTS_STUDY_PROFILES_LIST_CONFIG;
 
  private dataSubscription: Subscription;
  @ViewChild('model') model: AdvancedTableComponent;
  public recordsTotal: any;
  private studentId: any;

  constructor(private _translate: TranslateService,
              private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService) {
  }

async ngOnInit() {
    this._activatedTable.activeTable = this.model;
    this.dataSubscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      // find studentUser
      const user = await this._context.model('Students').asQueryable()
        .where('id').equal(this.studentId)
        .expand('studyProgram')
        .select('user')
        .getItem();

      try {
        if (user.user) {
          // get all user profiles
          this.model.query = await this._context.model('Students').asQueryable()
            .where('user').equal(user.user)
            .prepare();
        }

        this.model.config = AdvancedTableConfiguration.cast(STUDENTS_STUDY_PROFILES_LIST_CONFIG);
        this.model.fetch();

      } catch (err) {
        console.log(err);
      }

    });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    console.log(data);
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
