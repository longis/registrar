import {Component, OnInit, Input, OnDestroy, SimpleChanges} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AppEventService } from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-overview-graduation',
  templateUrl: './students-overview-graduation.component.html'
})
export class StudentsOverviewGraduationComponent implements OnInit, OnDestroy  {

  public student;
  @Input() studentId: number;
  private subscription: Subscription;
  private reloadSubscription: Subscription;
  private changeSubscription: Subscription

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext, private _appEvent: AppEventService) {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      this.fetchData(changes.studentId.currentValue).then(() => {
      }).catch(err => {
        console.error(err);
      });
    }
  }


  async ngOnInit()  {
    this.reloadSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload' && this.studentId) {
        this.fetchData(this.studentId).then(() => {
        }).catch(err => {
          console.error(err);
        });
      }
    });
    this._appEvent.changed.subscribe(change => {
      if (change && change.model === 'UpdateStudentStatusActions') {
        this.fetchData(this.studentId).then(() => {
        }).catch(err => {
          console.error(err);
        });
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  fetchData(studentId: number | string): Promise<any> {
    return this._context.model('Students')
      .where('id').equal(studentId)
      .expand('person($expand=gender), department, studyProgram, user, graduationPeriod($expand=locale)')
      .getItem()
      .then((value) => {
        this.student = value;
        // check if student is declared and get info from studentDeclaration model
        if (this.student && this.student.studentStatus && this.student.studentStatus.alternateName === 'declared') {
          return this._context.model('StudentDeclarations')
            .where('student').equal(this.studentId)
            .getItem().then((declaredInfo) => {
            if (declaredInfo) {
              this.student.declaredInfo = declaredInfo;
            }
          });
        }
      });
  }
}
