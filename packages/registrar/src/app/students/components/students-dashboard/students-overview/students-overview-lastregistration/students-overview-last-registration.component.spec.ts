import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsOverviewLastRegistrationComponent } from './students-overview-last-registration.component';

describe('StudentsOverviewLastRegistrationComponent', () => {
  let component: StudentsOverviewLastRegistrationComponent;
  let fixture: ComponentFixture<StudentsOverviewLastRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsOverviewLastRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewLastRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
