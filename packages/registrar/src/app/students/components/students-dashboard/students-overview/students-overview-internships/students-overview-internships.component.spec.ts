import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsOverviewInternshipsComponent } from './students-overview-internships.component';

describe('StudentsOverviewInternshipsComponent', () => {
  let component: StudentsOverviewInternshipsComponent;
  let fixture: ComponentFixture<StudentsOverviewInternshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsOverviewInternshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewInternshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
