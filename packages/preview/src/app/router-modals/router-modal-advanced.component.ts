import {Component, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-router-modal-advanced',
    templateUrl: './router-modal-advanced.component.html',
    styles: [`
        .tab-pane .card {
            box-shadow: none;
          }
        .tab-pane .card .hljs {
            background: #FFF;
        }
    `],
    encapsulation: ViewEncapsulation.None
})
export class RouterModalAdvancedComponent  {
}
